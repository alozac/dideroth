package ConstructionCartes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

abstract class ConstructionCartes {
	File fic;
	BufferedWriter f;
	int taille = 100;
	String[][] carte = new String[taille][taille];
	
	
	public ConstructionCartes(String nomFic){
		fic = new File("src/FichiersCartes/" + nomFic);
		try {
			f=new BufferedWriter(new FileWriter(fic));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public static void main(String[] args) throws IOException{
		Foret m = new Foret();
		m.remplirCarte();
	}
	
	public void remplirCarte() throws IOException{
		for (int i = 0; i<taille;i++){
			for (int j = 0; j<taille; j++){
			f.write(carte[i][j]);
			}
			f.write("\n");
		}
	f.close();
	}
	
	public void faireForet (int x,int y, int lgueur, int haut, String type){
		String [][] foret = new String[haut][lgueur];
		int xHaut = x+haut;
		int yLong = y+lgueur;
		for (int i = 0; i<haut; i++)
		for (int j = 0; j < lgueur; j++)
			 foret[i][j] = type;
		for (int i = x; i<xHaut; i++)
		for (int j = y; j <yLong; j++)
			carte[i][j] = foret[i-x][j-y];
	}
	
	public void planEau(int x, int y, int lgueur, int haut){
		String [][] eau = new String[haut][lgueur];
		int xHaut = x+haut;
		int yLong = y+lgueur;
		for (int i = 0; i<haut; i++)
		for (int j = 0; j < lgueur; j++)
			 eau[i][j] = "l2 ,";
		for (int i = x; i<xHaut; i++)
		for (int j = y; j <yLong; j++)
			carte[i][j] = eau[i-x][j-y];	
		ajouterBords();
	}
	
	public void ajouterBords(){
		ajouterCoins();
		ajouterBordsDroits();
		corrigerCoins();
	}
	public void ajouterBordsDroits(){
		for (int i = 0; i<taille; i++){
			for (int j = 0; j <taille; j++){
				if(!carte[i][j].equals("l2 ,") && estBord(i,j)) {
					if(i+1<taille && carte[i+1][j].equals("l2 ,"))carte[i][j]="l3 ,";
					if(j-1>=0 && carte[i][j-1].equals("l2 ,")) carte[i][j]="l4 ,";
					if(i-1>=0 && carte[i-1][j].equals("l2 ,")) carte[i][j]="l5 ,";
					if(j+1<taille && carte[i][j+1].equals("l2 ,")) carte[i][j]="l6 ,";
				}
			}
		}
	}

	public void ajouterCoins(){
		for (int i = 0; i<taille; i++){
			for (int j = 0; j <taille; j++){
				if(!carte[i][j].equals("l2 ,") && estBord(i,j)) {
					if(i+1<taille && j+1<taille && carte[i+1][j+1].equals("l2 ,"))carte[i][j]="l7 ,";
					if(i+1<taille && j-1>=0 && carte[i+1][j-1].equals("l2 ,")) carte[i][j]="l8 ,";
					if(i-1>=0 && j-1>=0 && carte[i-1][j-1].equals("l2 ,")) carte[i][j]="l9 ,";
					if(i-1>0 && j+1<taille && carte[i-1][j+1].equals("l2 ,")) carte[i][j]="l10 ,";
				}
			}
		}
	}
	
	public void corrigerCoins(){
		String rebord = "l([3456789]|10) ,";
		for (int i = 0; i<taille; i++){
			for (int j = 0; j <taille; j++){
				if (carte[i][j].matches("l[3|4|5|6] ,")
			&& ((j+1<taille && i-1>=0 && carte[i][j+1].matches(rebord) &&  carte[i-1][j].matches(rebord))
			|| (i+1<taille && j+1<taille && carte[i][j+1].matches(rebord) &&  carte[i+1][j].matches(rebord))
			|| (i-1>=0 && j-1>=0 && carte[i-1][j].matches(rebord) &&  carte[i][j-1].matches(rebord))
			|| (i+1<taille && j-1>=0 && carte[i][j-1].matches(rebord) &&  carte[i+1][j].matches(rebord)))) carte[i][j]="l2 ,";}}
	}
	
	
	private boolean estBord(int x, int y) {
		boolean eauVoisine = false;
		boolean solVoisin = false;
		for (int i =-1; i<2; i++){
			for (int j = -1; j <2; j++){
				if (x+i>=0 && y+j>=0 && x+i<taille && y+j<taille){
					if (carte[x+i][y+j].equals("l2 ,")) eauVoisine = true;
					else  solVoisin = true;
				}
			}
		}
		return eauVoisine && solVoisin;
	}
}
