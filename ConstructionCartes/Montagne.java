package ConstructionCartes;

import java.io.IOException;


public class Montagne extends ConstructionCartes {
	public Montagne (){
		super("Montagne.map");
	}
	
	public void remplirCarte() throws IOException{
		int alea=0;
		for (int i = 0; i<taille; i++){
			for (int j = 0; j<taille; j++){
				carte[i][j] = "";
				alea=(int)(Math.random()*200);
				if (alea==2) carte[i][j]+="m2 ,";
				else if (alea>=3 && alea<=8) carte[i][j]+="m5 ,";
				else if ((alea !=2 && alea <3) || alea >8)carte[i][j]+="m1 ,";
			}
		}
		planEau(15,32,12,17);
		super.remplirCarte();
	}
}
