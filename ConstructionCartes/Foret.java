package ConstructionCartes;
import java.io.IOException;

public class Foret extends ConstructionCartes{
	public Foret(){
		super("Foret.map");

	}
	
	public void remplirCarte() throws IOException{
		int alea=0;
		for (int i = 0; i<taille; i++){
			for (int j = 0; j<taille; j++){
				carte[i][j] = "";
				alea=(int)(Math.random()*200);
				if (alea==2) carte[i][j]+="f2 ,";
				else if (alea>=3 && alea<=8) carte[i][j]+="f6 ,";
				else if ((alea !=2 && alea <3) || alea >8)carte[i][j]+="f1 ,";
			}
		}
		planEau(26,13,12,14);
		planEau(26,15,20,10);
		planEau(79,67,22,11);
		planEau(56,49,20,20);
		faireForet(50,80,10,10, "f7 ,");
		super.remplirCarte();
	}
}