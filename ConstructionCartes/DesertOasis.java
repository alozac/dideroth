package ConstructionCartes;

import java.io.IOException;

public class DesertOasis extends ConstructionCartes{	
	
	public DesertOasis(){
		super("Desert.map");
	}
	
	public void remplirCarte() throws IOException {
		int alea=0;
		for (int i = 0; i<taille; i++){
			for (int j = 0; j<taille; j++){
				carte[i][j] = "";
				alea=(int)(Math.random()*20);
				if (alea<=2 && alea>0) carte[i][j]+="d6 ,";
				else if (alea==0) carte[i][j]+="d8 ,";
				else carte[i][j]+="d2 ,";
			}
		}
		planEau(30,30,10,10);
		planEau(35,35,8,7);
		planEau(27,33,5,14);
		planEau(2,39,4,10);
		planEau (50,50,1,1);
		planEau (54,50,1,1);
		super.remplirCarte();

	}

}
