Jeu de stratégie au tour par tour réalisé dans le cadre d'un projet universitaire en Java.
Le joueur a la possibilité de jouer seul contre une IA ou en coopération avec un second joueur.
Le but est de créer des unités et bâtiments afin d'éliminer le château du joueur adverse