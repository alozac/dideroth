package Modele;

/**
 * Humain est la classe qui implémente la tribu des humains. Les Humains sont 50% plus fort au corps a corps que les autres unités.
 * Elle hérite de la classe Tribu
 * @see Tribu
 */
public class Humain extends Tribu {

	/**
	 * Constructeur de Humain
	 */
	public Humain() {
		super("Humain", 0, 0.5);
	}

	/**
	 * Réinitialise le nombre de points de vie de l'unité en argument au maximum
	 * @param u l'unité dont les points de vie seront au maximum
	 */
	@Override
	void ajoutePdvSuppl(Unite u) {
		u.pdv=u.pdvMax;
		
	}

	/**
	 * Augmente les points de dégâts de l'unité de combat passée en argument, seulement s'il s'agit d'une unité de combat au corps à corps
	 * @param u une unité de combat
	 * @see Unite#getPdd()
	 * @see Unite#setPdd(int)
	 */
	@Override
	void ajouteAttaqueSuppl(UniteCombat u) {
		if (u instanceof CorpsACorps){
			int nouvAtt = (int) (u.getPdd() + u.getPdd() * bonusAttaque);
			u.setPdd(nouvAtt);
		}	
	}
}
