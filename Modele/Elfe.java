package Modele;

/**
 * Elfe est la classe implémentant la tribu des elfes. Les Elfes sont 50% plus forts à distance que les autres unités.
 * Elle hérite de la classe Tribu
 * @see Tribu
 */
public class Elfe extends Tribu {
		
		/**
		 * Constructeur d'Elfe
		 */
		public Elfe(){
			super("Elfe", 0, 0.5);
		}

		/**
		 * Réinitialise le nombre de points de vie de l'unité en argument au maximum
		 * @param u l'unité dont les points de vie seront au maximum
		 */
		@Override
		void ajoutePdvSuppl(Unite u) {
			u.pdv=u.pdvMax;
		}

		/**
		 * Augmente les points de dégâts de l'unité de combat passée en argument, seulement s'il s'agit d'une unité de combat à distance
		 * @param u une unité de combat
		 * @see Unite#getPdd()
		 * @see Unite#setPdd(int)
		 */
		@Override
		void ajouteAttaqueSuppl(UniteCombat u) {
			if (u instanceof ADistance){
				int nouvAtt = (int) (u.getPdd() + u.getPdd() * bonusAttaque);
				u.setPdd(nouvAtt);
			}
		}
}
