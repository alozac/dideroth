package Modele;

/**
 * CombatUVB est la classe implèmentant le combat d'une unité contre un bâtiment
 * Elle hérite de la classe Combat
 * @see Combat
 */
public class CombatUVB extends Combat {
	
	private UniteCombat allie;
	private Batiment cible;
	
	/**
	 * Constructeur de CombatUVB
	 * @param all une unité de combat alliée
	 * @param c un bâtiment visé par l'attaque de l'unité alliée
	 */
	public CombatUVB(UniteCombat all, Batiment c){
		super(all);
		allie = (UniteCombat) super.allie;
		cible=c;
	}

	/**
	 * Effectue l'attaque de l'unité sur le bâtiment et complète le rapport de combat
	 * @see UniteCombat#attaquer(Unite)
	 * @see Combat#uniteHTML(Unite, String)
	 */
	@Override
	void effectuer_combat() {
		int nbC=allie.nbCoups;
		int degat=allie.pdd;
		for (int i=0; i<nbC ; i++){
			allie.attaquer(cible);
		}
		rapport+=uniteHTML(allie, coulAll) + " a infligé <b>" + nbC + "*" + degat+"</b> points de dégâts à " + uniteHTML(cible, coulEnn) +".<br>";
		rapport+=cible.estMort()?uniteHTML(cible, coulEnn) + " a été détruit(e)!<br><br>" : "<br>";
	}

}
