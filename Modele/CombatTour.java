package Modele;

import java.util.ArrayList;

/**
 * CombatTour est la classe implémentant les coups portés par les tours (bâtiments) d'un joueur sur les unités ennemis étant dans sur leur portée d'attaque
 * Elle hérite de la classe Combat
 * @see Combat
 */
public class CombatTour extends Combat {

	private Tour allie;
	
	/**
	 * casesAAttaquer est une liste de cases contenant les emplacements que la tour peut attaquer
	 */
	private ArrayList<Case> casesAAttaquer = new ArrayList<Case>();
	
	/**
	 * Constructeur de CombatTour
	 * @param all une tour alliée
	 */
	public CombatTour(Tour all) {
		super(all);
		allie = all;
		Unite cible;
		for (Case c : allie.getCasesAPortee()){
			cible = c.getOccupant();
			if (!occupantAllie(cible) && cible != null) casesAAttaquer.add(c);
		}
	}
	
	/**
	 * Détermine si l'unité passée en paramètre est un allié de la Tour "allié"
	 * @param cible l'unité visée par l'attaque de la tour
	 * @see Tour#getJoueur()
	 * @see Joueur#getUnites()
	 * @see Joueur#getBatiments()
	 * @return true si l'unité visée est une alliée, false sinon
	 */
	public boolean occupantAllie(Unite cible){
		return allie.getJoueur().getUnites().contains(cible) || 
			   allie.getJoueur().getBatiments().contains(cible);
	}

	/**
	 * Effectue l'attaque de la tour et complète le rapport du combat
	 * @see Case#getOccupant()
	 * @see Tour#attaquer(Unite)
	 * @see Combat#uniteHTML(Unite, String)
	 * @see Tour#getPdd()
	 */
	@Override
	void effectuer_combat() {
		int nbVictimes = 0;
		if (casesAAttaquer.size() == 0) return;
		for (Case c : casesAAttaquer){
			if (allie.attaquer(c.getOccupant())) nbVictimes++; 
		}
		rapport += uniteHTML(allie, coulAll) + " a infligé <b>"+ allie.getPdd() + "</b> points de dégât à " + casesAAttaquer.size() + " ennemi(s).<br>";
		rapport += nbVictimes>0 ? (nbVictimes + (nbVictimes==1 ? 
										" ennemi a été tué!<br><br>"
										:" ennemis ont été tués!<br><br>" ))
								: "<br>";
	}

}
