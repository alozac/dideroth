package Modele;

/**
 * Recruter est une classe heritee d'Action. Elle permet de recruter des unites.
 */
public class Recruter extends Action {

	UniteCombat unit;
    /**
     * Le nombre des unites qui seront creees et le nombre de points d'action qui sera enleve lors une action
     */
	int quantite;
	
    /** 
     * Constructeur de Recruter
     * @param courant Le joueur courant
     * @param unite Un String correspondant au nom de l'unite qui sera creee
     * @param nbU correspond a la quantite d'unite que l'on va recruter
     */
	public Recruter(Joueur courant, String unite, int nbU) {
		super(courant, null);
		unit = UniteFactory.creerUnite(unite, j);;
		quantite = nbU;
	}
    /**
     * Effectue une action de reruter, ajoute l'unite avec la quantite donnee, enleve des points d'action au joueur
     * @see #ajouterUnites(Unite, int)
     * @see Joueur#depenserRess(int, Hashmap<String, Integer>)
     * @see Joueur#setPa(int)
     */
	public void effectuer_action() {
		ajouterUnites(unit , quantite );
		j.depenserRess(quantite, unit.getCout());
		j.setPa(quantite);
	}
    /**
     * Determine le nombre maximal du recrutement possible pour le joueur et l'unite donne
     * @param unit L'unite en question
     * @return Le minimum entre le nombre du recrutement possible du chateau et la depense maximale possible du joueur
     * @see Joueur#maxDepensePossible(HashMap<String, Integer>)
     * @see Joueur#getPa()
     * @see Chateau#nbEmplRecrutPossible()
     */
	public int determinerMaxPossible(UniteCombat unit){
		if (j.getChateau().getNiveau() < unit.getNivChateauNece()) return 0;
		int maxDep = j.maxDepensePossible(unit.getCout());
		return Math.min (j.getChateau().nbEmplRecrutPossible(), Math.min(maxDep, j.getPa()));
	}
    
    
	/**
     * Cree une unite et l'ajoute a la liste des unites du joueur courant
     * @param unit L'unite qui sera creee
     * @param qte La quantite des unites qui ont �t� cr�es
     * @see Joueur#ajouteUnite(Unite)
     * @see UniteFactory#creerUnite(Unite, int)
     */
	private void ajouterUnites(UniteCombat unit, int qte) {
		for (int i = 0 ; i<qte ; i++)
			j.ajouteUnite(UniteFactory.creerUnite(unit, j)); 
	}
}
