package Modele;

import java.util.HashSet;
import java.util.Set;

/**
 * Mouvement est la classe heritee d'Action permettant de deplacer les unites
 */
public class Mouvement extends Action{
   public Case depart;
   public Case arrivee;
   protected UniteCombat u;
   protected HashSet<Coordonnee> casesAPorteeMvt = new HashSet<Coordonnee>();
   protected HashSet<Coordonnee> casesAPorteeAtt = new HashSet<Coordonnee>();
   protected HashSet<Coordonnee> casesAPorteeSoins = new HashSet<Coordonnee>();
   
    /**
     * Constructeur de Mouvement 
     * @param j Le joueur qui effectue un mouvement
     * @param p Le plateau sur lequel le mouvement sera effectue
     * @param depart La case de depart de l'unite qui sera deplacee
     */
    public Mouvement (Joueur j, Plateau p, Case depart){
        super(j, p);
        this.depart = depart;
        Unite occ = depart.getOccupant();
        if (occ instanceof UniteCombat) u = (UniteCombat) occ;
        else u = null;
    }
    
    /**
     * Constructeur vide de Mouvement, ne prend que le plateau en argument
     * @param p
     */
    public Mouvement(Plateau plateau) {
        super(null, plateau);
    }
    
    /**
     * Effectue une action de mouvement, deplace l'unite sur la case d'arrivee
     * @see Case#setOccupant(Unite)
     * @see Joueur#setPa(int)
     */
    public void effectuer_action(){
	   arrivee.setOccupant(u);
       depart.setOccupant(null);
       j.setPa(1);
    }
    
    /**
     * Verifie si la case d'arrivee est a la portee du mouvement, si les points d'action de joueur sont suffisants et si la case de depart est occupee par un allie
     * @param deparrivee Le String indiquant s'il s'agit de la case de depart ou d'arrivee
     * @see Action#occupantAllie(Unite)
     * @return true si la case est correcte et false sinon
     */
    public boolean verifierCase(String deparrivee) {
        if (j.getPa() == 0) return false;
        if (deparrivee.equals("depart")) return u!=null && !u.aAttaque && occupantAllie(u);
        return casesAPorteeMvt.contains(arrivee.coord);
    }
    /**
     * Determine les cases a portee (de mouvement, d'attaque ou de soin) de l'unite effectuant le mouvement
     * @see #setCasesAPortee(int, HashSet<Case>)
     */
    public void setCasesAPortee(){
    	setCasesAPortee(u.getPorteeA(), new HashSet<Case>());
    }
    
    /**
     * Methode recursive permettant d'ajouter les cases a portee de l'unite
     * @param portee la portee de l'unite
     * @param dernAjout les cases ajoutees lors du dernier appel a cette methode
     */
    public void setCasesAPortee(int portee, HashSet<Case> dernAjout){
    	if (u.getPorteeA() == portee)dernAjout.add(depart);
    	HashSet<Case> tmp= new HashSet<Case>();
    	HashSet<Case> nouvAjout= new HashSet<Case>();
    	for (Case c : dernAjout)
    		tmp.addAll(definirCasesAdja(c));
    	for (Case c : tmp) {
    		if (portee  > u.getPorteeA() - u.getPorteeD())
    			ajoutCasesAPortee(c, nouvAjout, true);
    		else 
    			ajoutCasesAPortee(c, nouvAjout, false);
    	}
    	if (portee - 1 != 0) setCasesAPortee(portee-1, nouvAjout);
    	else casesAPorteeMvt.remove(depart.coord);
    }
    
    /**
     * Dtermmine les cases adjacentes a celle donnee en parametre
     * @param c la case dont on veut connaitre les voisines
     * @return les cases adjacentes a la case c
     */
	private Set<Case> definirCasesAdja(Case c) {
	   Set<Case> list = new HashSet<Case>();
		for (int i = -1 ; i<=1;i++){
		for (int j = -1 ; j<=1;j++){
			if (Math.abs(i+j) == 1){
				Case c2 = plateau.getCase(c.coord.x+i, c.coord.y+j);
				if (u instanceof UniteVolante) 
					if (c2 != null) {
						list.add(c2);
						continue;
					}
				if (c2 != null && c2.estFranchissable())  list.add(c2);
			}
		}
		}
		return list;
	}
	
	/**
     *Ajoute la case c a la bonne liste de casesAPortee (attaque, mouvement ou soin) en fonction du type de l'unite
	 * @param c la case a ajouter dans une liste
	 * @param nouvAjout les cases dernierement ajoutees a la liste casesAPorteeMvt
	 * @param aPorteeDepl determine si la case c est dans la portee de deplacement de l'unite ou non
	 */
	private void ajoutCasesAPortee(Case c, HashSet<Case> nouvAjout, boolean aPorteeDepl) {
		if (c.estLibre()){
			if (aPorteeDepl) casesAPorteeMvt.add(c.coord);
			nouvAjout.add(c);
		}
		else {
			if (!occupantAllie(c.getOccupant())) casesAPorteeAtt.add(c.coord);
			else if (u instanceof LanceurSorts) casesAPorteeSoins.add(c.coord);
			if (u instanceof UniteVolante) nouvAjout.add(c);
		}
	}
	
    /**
     * Modifie la case d'arrivee
     * @param arr La nouvelle case d'arrivee
     */
    public void setArrivee(Case arr) {
    	arrivee = arr;
    }
    
    /** 
     * Retourne a liste des cases a portee de mouvement
     * @return Les cases a portee de mouvement
     */
	public HashSet<Coordonnee> getCasesAPorteeMvt(){
    	return casesAPorteeMvt;
    }
    /**
     * Retourne a liste des cases a portee d'attaque
     * @return Les cases a portee d'attaque
     */
    public HashSet<Coordonnee> getCasesAPorteeAtt(){
    	return casesAPorteeAtt;
    }
    /**
     * Retourne a liste des cases a portee de soins
     * @return Les cases a portee de soins
     */
    public HashSet<Coordonnee> getCasesAPorteeSoins(){
    	return casesAPorteeSoins;
    }
}