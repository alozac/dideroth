package Modele;

import java.util.ArrayList;

/**
 * AttaqueEnnNeutres est la classe implémentant les attaques des monstres dans le mode Joueur versus Ennemi
 * Elle hérite de la classe Attaquer
 * @see Attaquer
 */
public class AttaqueEnnNeutres extends Attaquer {

	/**
	 * La liste des monstres pr�sents sur le plateau de jeu
	 */
	ArrayList<Monstre> listeMonstres;
	
	/**
	 * Constructeur d'AttaqueEnnNeutres
	 * @param listeM la liste de monstres
	 * @param plateau le plateau de jeu
	 */
	public AttaqueEnnNeutres(ArrayList<Monstre> listeM, Plateau plateau) {
		super(plateau);
		listeMonstres = listeM;
	}

	/**
	 * Permet d'effectuer l'attaque
	 * @see Mouvement#setCasesAPortee()
	 * @see CombatEnnNeutresVUnites#ecrireRapport()
	 */
	@Override
	public void effectuer_action() {
		for (Monstre m : listeMonstres) {
			depart = m.position;//System.out.println(depart.getOccupant());
			u = m;
			effacerCasesAPortee();
			setCasesAPortee();
			attaquerSiPossible();
		}
		rapport = CombatEnnNeutresVUnites.ecrireRapport();
	}

	/**
	 * Permet d'attaquer une cible si elle est située à portée d'attaque et qu'il ne s'agit pas d'un monstre
	 * @see Plateau#getCase(Coordonnee)
	 * @see Case#getOccupant()
	 * @see Mouvement#setArrivee(Case)
	 * @see Attaquer#deplacerAvantCombat()
	 * @see CombatEnnNeutresVUnites#effectuer_combat()
	 * @see UniteCombat#aAttaque()
	 */
	public void attaquerSiPossible(){
		for (Coordonnee c : casesAPorteeAtt) {
			cible = plateau.getCase(c).getOccupant();
			if (cible != null && !(cible instanceof Monstre)){
				setArrivee(cible.position);
				deplacerAvantCombat();
				CombatEnnNeutresVUnites combat = new CombatEnnNeutresVUnites((Monstre) u, cible);
				combat.effectuer_combat();
				u.aAttaque = true;
				return;
			}
		}
	}
	
	/**
	 * Permet d'effacer les cases à portée d'attaque
	 */
	private void effacerCasesAPortee() {
		casesAPorteeAtt.clear();
	}
}
