package Modele;

/**
 * CombatUVU est la classe implémentant un combat unité contre unité
 * Elle hérite de la classe Combat
 * @see Combat
 */
public class CombatUVU extends Combat {
	private UniteCombat allie;
	private UniteCombat cible;
	private boolean riposteEnnemi;
	
	/**
	 * Constructeur de CombatUVU
	 * @param all une unité de combat alliée
	 * @param cible une unité de combat adverse
	 */ 
	public CombatUVU(UniteCombat all, UniteCombat cible){
		super(all);
		allie = (UniteCombat) super.allie;
		this.cible=cible;
		riposteEnnemi = allie instanceof CorpsACorps && cible instanceof CorpsACorps;
		if (cible instanceof Monstre) 
			coulEnn  = "#206020";
	}
	
	/**
	 * Effectue le combat et complète le rapport de combat (indique quel coup est porté à qui, quelle esquive a été effectuée, quelles sont les victimes)
	 */	
	public void effectuer_combat(){
		int nbCAll = allie.nbCoups;
		int nbCEnn = cible.nbCoups;
		while (nbCAll > 0 || (nbCEnn > 0 && riposteEnnemi)){
			if (nbCAll>0){
				if(allie.attaquer(cible))	rapport+=uniteHTML(allie,coulAll) +" fait <b>"
														+allie.pdd +"</b> à " + uniteHTML(cible, coulEnn) + ".<br>";
				else 	rapport+=uniteHTML(cible, coulEnn) + " esquive " + uniteHTML(allie, coulAll) + ".<br>";
				if (cible.estMort()) 	{ 
					rapport+=uniteHTML(cible, coulEnn) + " est mort(e)!<br><br>";
					return;
				}
				nbCAll--;
			}
			if (riposteEnnemi && nbCEnn >0){
				if (cible.attaquer(allie)) rapport+=uniteHTML(cible, coulEnn) + " fait <b>"
														+cible.pdd+"</b> à " + uniteHTML(allie, coulAll) + ".<br>";
				else rapport+= uniteHTML(allie, coulAll) + " esquive " + uniteHTML(cible, coulEnn) + ".<br>";
				if (allie.estMort()) {
					rapport+= uniteHTML(allie, coulAll) + " est mort(e)!<br><br>";
					return;
				}
				nbCEnn--;
			}
		}
		rapport+="<br>";
	}
	
}

