package Modele;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Map.Entry;

/** 
 * Plateau est la classe représentant l'ensemble des cases sur lesquelles se déplacent les unités
 */
public class Plateau {
	/** 
     * Le tableau de tableaux de cases
     */
	Case[][] cases;
    /** 
     * La taille du plateau
     */
	public int taille;
    
    /**
     * Une HashMap avec les noms et les types de terrain correspondant
     */
    private HashMap<String, TypeTerrain> typesTerr = new HashMap<String, TypeTerrain>();
    
    /**
     * Construit le plateau de jeu
     * @param t la taille du plateau
     * @param chemin le chemin d'accès aux fichiers utiles à la construction des cases
     * @param typeCarte le type de carte choisi par les joueurs
     */
	public Plateau(int t, String chemin, String typeCarte){	
		taille = t;
		cases = new Case[taille][taille];
		
 		String s = null;
 		
		Scanner sc = null;
		try {
			sc = new Scanner(new FileInputStream(chemin + typeCarte + ".cfg"));
		} catch (FileNotFoundException e) {
			System.err.println(e);
		}
		
		while (sc.hasNextLine()) {
			s = sc.nextLine();
			typesTerr.put(s, TypeTerrainFactory.creerTerrain(s, sc.nextLine(), sc.nextLine(), sc.nextLine()));
		}
		
		Scanner scan = null;
		try {
			scan = new Scanner(new FileInputStream(chemin + typeCarte + ".map"));
		} catch (FileNotFoundException e) {}
		
		scan.useDelimiter("\\s*,\n*");
		for(int i= 0;i<taille;i++)
			for(int j= 0;j<taille;j++)
				cases[i][j] = new Case(new Coordonnee(i,j),typesTerr.get(scan.next()) );
	}
	
    /** 
     * Retourne une case du plateau avec les parametres donnes au argument
     * @param coord Les coordonnees de la case recherchee
     * @return cases[coord.x][coord.y] ou null si la coordonnee n'existe pas dans le plateau
     */
	public Case getCase(Coordonnee coord){
		try{
			return cases[coord.x][coord.y];
		} catch(ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}
	
    /** 
     * Retourne une case du plateau avec les parametres donnes au argument
     * @param x La coordonnee x de la case recherchee
     * @param y La coordonnee y de la case recherchee
     * @return La case recherchee ou null si les coordonnees passees en argument depassent la taille du plateau 
     */
	public Case getCase(int x, int y){
		try{
			return cases[x][y];
		}catch(ArrayIndexOutOfBoundsException e) { 
			return null;
		}
	}
	
    /**
     * Permet de récupérer les images des différents types de terrains utilisés sur le plateau
     * @return La HashMap avec les identifiants des types de terrain comme clé et le chemin de leur image en valeur
     */
	public HashMap<String, String> getImagesTypesTerr(){
		HashMap<String, String> im = new HashMap<String, String>();
		for (Entry<String, TypeTerrain> couple: typesTerr.entrySet())
			im.put(couple.getKey(), couple.getValue().image());
		return im;
	}

	/**
	 * Permet de redisposer des coffres aléatoirement sur la carte si besoin (si un ou plusieurs coffres ont été découverts par un des joueurs)
	 */
	public void nouveauxCoffres() {
		while (!Case.nbCoffresSuffisant()){
			int rd1 = (int)(Math.random()*taille);
			int rd2 = (int)(Math.random()*taille);
			getCase(rd1, rd2).ajoutRessourcesAlea();
		}
	}
	
}
