package Modele;

/** Marecage est la classe heritee de TypeTerrain
 * @see TypeTerrain
 */
public class Marecage extends TypeTerrain{
    /**
     * Construit un marecage
     * "super" correspond au constructeur de la classe TypeTerrain
     */

	public Marecage(String type, String image, String nom, boolean franchissable){
		super(type, image , nom, franchissable);
	}

}
