package Modele;
import java.util.ArrayList;
import java.util.Map.Entry;

/**
 * Chateau est la classe implémentant le château d'un joueur
 * Elle hérite de la classe Batiment
 * @see Batiment
 */
public class Chateau extends Batiment{

	/**
	 * dureeConstrNivSvt est l'entier égal au nombre de tours que mettra le chateau pour passer au niveau suivant
	 */
	int dureeConstrNivSvt;
	
	/**
	 * dureeConstrRestante est l'entier égal au nombre de tours restant avant la fin de la construction du château
	 * (nombre de tours au bout duquel le joueur pourra réaméliorer le chateau)
	 */
	int dureeConstrRestante;
	
	public boolean enConstruction = false;
	
	/**
	 * emplRecrut est une liste de cases correspondant aux emplacements des unités autour du château
	 */
	private final ArrayList<Case> emplRecrut;
	
	/**
	 * emplDispos est une liste de cases correspondant aux emplacements disponibles autour du château
	 */
	private ArrayList<Case> emplDispos= new ArrayList<Case>();
	
	/**
	 * Constructeur de Chateau
	 * @param j le joueur propriétaire du château
	 * @param p le plateau de jeu
	 * @param c le couple de coordonnées déterminant l'emplacement du château
	 * @see Unite#remplirCout(int, int, int)
	 */
	public Chateau(Joueur j, Plateau p, Coordonnee c){
		super (p, 200, j, 0, 2, 200);
		remplirCout(2000,5,5);
		setPosition(c);
		emplRecrut = determinerEmplRecrut(p);
		emplDispos.addAll(emplRecrut);
		dureeConstrRestante = 0;
		dureeConstrNivSvt = 0;
	}
	
	/**
	 * Retourne une liste de cases contenant les emplacements des unités autour du château 
	 * @param p le plateau de jeu
	 * @return les emplacements autour du chateau
	 * @see Plateau#getCase(int, int)
	 * @see Case#estLibre()
	 */
	public ArrayList<Case> determinerEmplRecrut(Plateau p){
		int taille = p.taille;
		ArrayList<Case> list = new ArrayList<Case>();
		for (Case c : position){
			int x=c.coord.x;
			int y=c.coord.y;
			for (int i = -1;i<2;i++){
			for (int j = -1 ; j<2 ; j++){
				Case tmp;
				if (!(j==0 && i==0) && x+i>=0 && x+i<taille && y+j>=0 && y+j<taille) tmp = p.getCase(x + i,y + j);
				else continue;
				if (tmp.estLibre() && !list.contains(tmp)) list.add(tmp);
			}
			}
		}
		return list;
	}
	
	/**
	 * Permet d'augmenter le niveau du château 
	 */
	public void augmenterNiveau() {
		niveau++;
		for (Entry<String, Integer> s: cout.entrySet())
			s.setValue(s.getValue() * (niveau+1));
		dureeConstrRestante = dureeConstrNivSvt;
		enConstruction = true;
		if ((niveau+1)%2 == 1) dureeConstrNivSvt++;	
		productionParTour*=2;
	}
	
	/**
	 * Ajoute une unité de combat sur un emplacement disponible autour du château
	 * @param unit l'unité de combat à ajouter parmi les recruts
	 */
	public void ajouteUnite(UniteCombat unit){
		Case empl = emplDispos.get(0);
		empl.setOccupant(unit);	  
		emplDispos.remove(0);
	}

	/**
	 * Met la liste des emplacements de recruts à jour. Ajoute les emplacements vides à la liste des emplacements disponibles
	 * et ajoute les emplacements occupés à la liste des emplacements de recruts
	 * @see Case#estLibre()
	 */
	public void miseAJourEmplRecrut(){
		for (Case c : emplRecrut) {
			if (c.estLibre() && !emplDispos.contains(c)) emplDispos.add(c);
			if (!c.estLibre() && emplDispos.contains(c)) emplDispos.remove(c);
		}
	}
	
	/**
	 * Finalise la construction du château et augmente les ressources en or du joueur à la fin du tour 
	 * @see Joueur#ajouterRess(String, Integer)
	 */
	public void finDeTour(){
		if (dureeConstrRestante == 0) enConstruction = false;
		if (dureeConstrRestante > 0) dureeConstrRestante--;
		joueur.ajouterRess("Or", productionParTour);
	}
	
	/**
	 * Retourne le nombre d'emplacements d'unités possible autour du château
	 * @return int
	 */
	public int nbEmplRecrutPossible(){
		return emplDispos.size();
	}
	
	/**
	 * Retourne une chaîne de caractère indiquant le niveau du château
	 * @return String
	 */
	public String toString(){
		return "Chateau lvl." + niveau;
	}
	
	/**
	 *Retourne une chaine de caractères utilisée pour aller chercher les images de chateau qui seront visibles sur la carte
	 * @return String 
	 */
	@Override
	public String identImage() {
		return "chateau";
	}

}
