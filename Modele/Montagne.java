package Modele;

/**
 * Montagne est une classe heritant TypeTerrain
 * @see TypeTerrain
 */

public class Montagne extends TypeTerrain {
    
    /**
     * Construit une montagne
     * "super" correspond au constructeur de la classe TypeTerrain
     * @param type l'identifiant du type de terrain
	 * @param image le chemin d'acces a l'image représentant le type de terrain
	 * @param nom le nom du type de terrain
	 * @param franchissable indique si le type de terrain est franchissable par les unites ou non
     */
	public Montagne(String type, String image, String nom, boolean franchissable) {
		super(type, image , nom, franchissable);
	}

}
