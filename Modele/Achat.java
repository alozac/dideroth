package Modele;

/**
 * Achat est la classe utilisée pour acheter des bâtiments supplémentaires en cours de partie
 * Cette classe dérive de la classe Action
 * @see Action
 */
 
public class Achat extends Action{
	
	/**
	 * Le bâtiment bat correspond à celui que le joueur veut acquérir dans la fenêtre d'achat
	 */	
	public Batiment bat;
	
	/**
	 * La coordonnée pos correspond à l'emplacement sur lequel le joueur souhaite construire le bâtiment bat
	 */
	Coordonnee pos;
	
	/**
	 * Constructeur d'Achat
	 * @param p le plateau de jeu
	 * @param j le joueur effectuant l'achat
	 * @param b la chaîne de caractère indiquant quel type de bâtiment le joueur achète
	 */
	public Achat (Plateau p, Joueur j, String b){
		super (j, p);
		bat = BatimentFactory.creerBatiment(p, b, j);
	}

	/**
	 * Permet d'effectuer l'achat du bâtiment et de le placer sur le plateau
	 * @see Joueur#getBatiments()
	 * @see Joueur#setPa(int)
	 * @see Batiment#setPosition(Coordonnee) 
	 */
	@Override
	public void effectuer_action() {
		j.getBatiments().add(bat);
		bat.setPosition(pos);
		j.depenserRess(1, bat.getCout());
		j.setPa(1);
	}
	
	/**
	 * Détermine si l'achat du batiment est possible
	 * @return true si les conditions suivantes sont toutes satisfaites :
	 * le niveau du château du joueur permet l'achat
	 * le joueur a les ressources en or nécessaires pour l'achat
	 * le joueur a encore au moins 1 point d'action pour pouvoir effectuer l'achat
	 * Retourne false sinon 
	 */
	public boolean achatPossible(){
		return bat.getNivChateauNece() <= j.getChateau().getNiveau() && j.maxDepensePossible(bat.getCout())>0 && j.getPa()>0;
	}

	/** 
	 * Détermine si la construction du batiment est possible sur la case de coordonnées passées en parametre
	 * @param c la coordonnee correspondant à l'emplacement souhaité du bâtiment
	 * @return true s'il est possible de construire le bâtiment acheté sur l'emplacement indiqué, false sinon
	 * @see Case#estLibre()
	 * @see Case#estFranchissable()
	 */
	public boolean constructionPossible(Coordonnee c){
		int tailleBat = bat.tailleBat;
		for (int i=0;i<tailleBat;i++){
			for (int j=0;j<tailleBat;j++){
				Case c2 = plateau.getCase(c.x + i, c.y + j);
				if (c2 == null || !c2.estLibre() || !c2.estFranchissable()
				|| !estDansMiPlateau(c.x + i)) return false;
			}
		}
		pos = c;
		return true;
	}
	
	/**
	 * Détermine si la x-ième ligne est inclue dans la moitié de plateau possédée par le joueur j (moitié supérieure pour le joueur 1, inférieure pour le joueur 2)
	 * @param x un entier correpondant à la x-ième ligne du plateau
	 * @return true si l'entier se trouve dans la bonne moitié du plateau, false sinon
	 */
	public boolean estDansMiPlateau(int x){
		if (j.getNum()==1) return x < plateau.taille/2;
		return x > plateau.taille/2;
	}
	
	
	

}
