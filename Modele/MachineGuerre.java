package Modele;

/**
 * Une MachineGuerre est une unite se caracterisant par une faible portee de deplacement mais une forte portee d'attaque 
 * MachineGuerre est une classe heritant ADistance
 */
public class MachineGuerre extends ADistance{
    /**
     * Contruit une machine de guerre en prenant un joueur en argument
     * @param j Le joueur auquel appartiendra la machine de geurre
     */
	public MachineGuerre(Joueur j){
    	//pv, pdd, nbCoups, agi, porteeD, porteeA, nivNece, joueur, soin
		super (40, 9, 1, 0, 3, 7, 2, j);
		remplirCout(300,7,7);
	}
	/**
	 * Retourne la représentation textuelle d'une MachineGuerre
     * @return Un String indiquant qu'il s'agit d'une machine de geurre
     */
    public String toString(){
		return "Machine de Guerre";
	}
    /**
     * Retourne une chaine permettant de retrouver l'image correspondant a l'unite
     * @return Un String correspondant au nom de l'image de la machine de guerre
     */
    public String identImage(){
    	return "Machine";
    }
  
}
