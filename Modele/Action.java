package Modele;

/**
 * Action est la classe abstraite permettant au joueur d'effectuer diverses actions durant la partie 
 */
public abstract class Action {
	protected Joueur j;
	protected Plateau plateau;
	
	/**
	 * Constructeur d'Action
	 * @param j le joueur effectuant l'action
	 * @param p le plateau de jeu
	 */
	public Action (Joueur j, Plateau p){
		plateau = p;
		this.j = j;
	}
	
	abstract void effectuer_action();

	/**
	 * Détermine si cible est un allié du joueur j ou non
	 * @param cible l'unité dont on veut déterminer le camp
	 * @return true si la cible est une unité alliée, false sinon
	 */
	public boolean occupantAllie(Unite cible) {
		if (j == null) return cible instanceof Monstre;
		return j.getUnites().contains(cible) || j.getBatiments().contains(cible);
	}
	
}
