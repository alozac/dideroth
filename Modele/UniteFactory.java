package Modele;
/** 
 * UniteFactory est une classe permettant de creer des unites
 */
public class UniteFactory {
    /**
     * Cree une nouvelle unite par rapport aux parametres donnees en argument
     * @param typeUnite Le String correspondant au type d'unite qui sera cree
     * @param j Le joueur auquel elle appartiendra
     * @return La nouvelle unite
     */
    public static UniteCombat creerUnite(String typeUnite, Joueur j) {
		switch(typeUnite.toLowerCase()){
		case "archer":
			return new Archer(j);
		case "chevalier":
			return new Chevalier(j);
		case "machineguerre":
		case "machine de guerre":
		case "machine":
			return new MachineGuerre(j);
		case "magicien":
			return new Magicien(j);
		case "dragon":
			return new UniteVolante(j);
		}
		return null;
	}
    /**
     * Cree une nouvelle unite par rapport aux parametres donnees en argument
     * @param unit L'unite qui sera cree
     * @param j Le joueur auquel elle appartiendra
     * @return La nouvelle unite
     */
	public static UniteCombat creerUnite(UniteCombat unit, Joueur j) {
		if (unit instanceof Archer)
			return new Archer(j);
		if (unit instanceof Chevalier)
			return new Chevalier(j);
		if (unit instanceof MachineGuerre)
			return new MachineGuerre(j);
		if (unit instanceof Magicien)
			return new Magicien(j);
		if (unit instanceof UniteVolante)
			return new UniteVolante(j);
		return null;
	}

	
}