package Modele;

/**
 * Magicien est la classe heritee de LanceurSorts, il a le pouvoir de soigner ses allies
 */
public class Magicien extends LanceurSorts{
    /**
     * Contruit un magicien avec les paramtres appropries pour celui-ci
     * @param j joueur auquel appartiendra le magicien
     * @see Unite#remplirCout(int, int, int)
     */
    public Magicien (Joueur j){
    	//pv, pdd, nbCoups, agi, porteeD, porteeA, nivNece, joueur, soin
    	super (20, 7, 1, 0.1, 5, 9, 2, j, 2);
    	remplirCout(140,7,7);
    }
    /**
     * Retourne la representation textuelle de l'unite
     * @return Un String indiquant qu'il s'agit d'un magicien
     */
    public String toString(){
		return "Magicien";
	}
}
