package Modele;

/**
 * Foret est la classe qui implémente la carte forêt 
 * Elle hérite de la classe TypeTerrain
 * @see TypeTerrain
 */
public class Foret extends TypeTerrain{

	/**
	 * Constructeur de Foret
	 * @param type une chaîne de caractères identifiant le type de terrain
	 * @param image une chaîne de caractères indiquant le nom de l'image à afficher
	 * @param nom une chaîne de caractères indiquant le nom de la carte
	 * @param franchissable un booléen indiquant s'il est possible ou non de franchir le type de terrain en question
	 */
	public Foret(String type, String image, String nom, boolean franchissable){
		super(type, image , nom, franchissable);
	}	

}
