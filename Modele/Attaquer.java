package Modele;

/**
 * Attaquer est la classe implémentant les différentes méthodes d'attaque
 * Elle hérite de la classe Mouvement
 * @see Mouvement
 */
public class Attaquer extends Mouvement {

	/**
	 * cible correspond à l'unité visée par l'attaque
	 */
	protected Unite cible;
	
	/**
	 * rapport est une chaîne de caractères décrivant l'attaque
	 */
	public String rapport = "";
	
	/**
   * Constructeur d'Attaquer
   * @param mvt correspondant au mouvement effectué par l'attaquant
   * @param cib l'unité cible de l'attaque
	 */
	public Attaquer(Mouvement mvt, Unite cib) {
		super (mvt.j, mvt.plateau, mvt.depart);
		casesAPorteeAtt = mvt.casesAPorteeAtt;
		casesAPorteeSoins = mvt.casesAPorteeSoins;
		casesAPorteeMvt = mvt.casesAPorteeMvt;
		cible = cib;
		arrivee = mvt.arrivee;
	}
	
	/**
	 * Constructeur d'Attaquer
	 * @param plateau le plateau de jeu
	 */
	public Attaquer(Plateau plateau) {
		super(plateau);
	}

	/**
	 * Permet d'effectuer une attaque
	 */
	@Override
	public void effectuer_action() {
		Combat c = definirTypeCombat();
	  c.effectuer_combat();
	  rapport = c.rapport;
	  u.aAttaque = true;
	  if (j != null) j.setPa(1);
	}
	
	/**
	 * Définit le type de combat à adopter en fonction de la nature de la cible :
	 * s'il s'agit d'un allié, un magicien le soigne
	 * @see Soins
	 * si l'attaquant est un combattant au corps à corps, appel de la méthode à suivre
	 * si la cible est un bâtiment, retourne un combat unité contre bâtiment
	 * @see CombatUVB
	 * si la cible est une unité, retourne un combat unité contre unité
	 * @see CombatUVU
	 * @return Combat le type de combat a effectuer
	 */
	public Combat definirTypeCombat(){
		if (occupantAllie(cible)) return new Soins((LanceurSorts) u, cible); 	
		if (u instanceof CorpsACorps) deplacerAvantCombat();
		if (cible instanceof Batiment) return new CombatUVB(u, (Batiment) cible);
		if (cible instanceof UniteCombat) 
			return new CombatUVU (u, (UniteCombat) cible);
		return null;
	}
	
	/**
	 * Permet à une unité de type Corps a Corps de se déplacer à côté de sa cible avant de l'attaquer
	 */
	public void deplacerAvantCombat() {
		int x = arrivee.coord.x;
		int y = arrivee.coord.y;
		for (int i = -1 ; i<=1;i++){
		for (int j = -1 ; j<=1;j++){
			if (Math.abs(i+j) == 1){
				Case c = plateau.getCase(x+i,y+j);
				if (c != null && casesAPorteeMvt.contains(c.coord)) {
					c.setOccupant(u);
					depart.setOccupant(null);
					return;
				}
			}
		}
		}
	}

	/**
	 * Détermine si la case de départ (ou d'arrivée) est correcte
	 * @return true si la case est correcte, false sinon
	 */
	public boolean verifierCase(String deparrivee) {
		if (u instanceof CorpsACorps || u instanceof ADistance)
			return !occupantAllie(cible) && casesAPorteeAtt.contains(arrivee.coord) ; 
		if (u instanceof LanceurSorts)
			return (casesAPorteeAtt.contains(arrivee.coord) || casesAPorteeSoins.contains(arrivee.coord));
		return false;
	}
}
