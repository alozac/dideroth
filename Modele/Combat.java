package Modele;

/**
 * Combat est la classe implémentant le combat au sein du jeu
 */ 
abstract class Combat {

	protected String rapport;
	
	/**
	 * coulAll et coulEnn sont des chaînes de caractères désignant respectivement la couleur des alliés et celle de l'ennemi
	 */
	protected String coulAll, coulEnn;

	protected Unite allie;
	
	/**
	 * Constructeur de Combat
	 * @param all une unité alliée
	 * @see Unite#getJoueur()
	 * @see Joueur#getCoulJoueur()
	 */	
	public Combat(Unite all){
		allie = all;
		rapport = "";
		coulAll = allie.getJoueur().getCoulJoueur().equals("Bleu")?"blue":"red";
		coulEnn = coulAll.equals("blue")?"red":"blue";
	}
	
	/**
	 * Constructeur vide de Combat
	 */	
	public Combat() {
		rapport = "";
	}

	abstract void effectuer_combat();
	
	/**
	 * Renvoie le rapport
	 */
	public String toString(){ return rapport;}
	
	public String uniteHTML(Unite u, String coul){
		return "<font color="+coul+">"+u.toString() + "</font>";
	}
		
}
