package Modele;

import java.util.HashMap;

public class Case {
	
	private static int nbCoffres, nbCoffresTotal;
	private TypeTerrain terrain;
	private Unite occupant;
	public Coordonnee coord;
	private String[] typesRess = {"Or", "Bois", "Minerai"};
	private boolean existeRessources = false;
	private HashMap<String, Integer> ressources;
	
	public Case (Coordonnee c, TypeTerrain t){
		terrain = t;
		coord = c;
		occupant = null;
		ressources = new HashMap<String, Integer>();
		int rd=(int)(Math.random()*1000);
		if (rd<=6){
			ajoutRessourcesAlea();
		}
		nbCoffresTotal = nbCoffres;
	}

	public void ajoutRessourcesAlea() {
		if (estFranchissable() && !existeRessources && estLibre()){
			for (int i = 0 ; i < typesRess.length ; i++){
				if (typesRess[i].equals("Or")){
					int rd1 = (int)(Math.random()*400)+100;
					ressources.put(typesRess[i], rd1);
				}else{
					int rd = (int)(Math.random()*4)+1;
					ressources.put(typesRess[i], rd);
				}
			}
			nbCoffres++;
			existeRessources = true;
		}
	}

	public boolean estLibre(){
		return occupant==null;
	}

	public void setOccupant(Unite u){
		occupant=u;
		if (u!=null){
			u.setPosition(this);
			if (u instanceof Monstre) return;
			if (u instanceof UniteCombat){
				if (existeRessources) nbCoffres--;
				for (int i = 0 ; i < typesRess.length ; i++){
					if (ressources.containsKey(typesRess[i])) u.joueur.ajouterRess(typesRess[i], ressources.get(typesRess[i]));
					ressources.remove(typesRess[i]);
					existeRessources = false;	
				}
			}
		}
	}

	public Unite getOccupant(){
		return occupant;
	}
	public TypeTerrain getTerrain(){
		return terrain;
	}
	public String toString(){
		 return occupant==null?" ":occupant.toString();
	}
	public boolean estFranchissable(){
		return terrain.getFranchissable();
	}
	public boolean existeRessources(){
		return existeRessources;
	}
	public static boolean nbCoffresSuffisant(){
		return nbCoffres == nbCoffresTotal;
	}

}
