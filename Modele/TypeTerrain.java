/**
 * TypeTerrain est la classe qui indique quel type de terrain devra etre choisi pour une case
 */
package Modele;

public class TypeTerrain {

	private String id;
	private String image;
	private String nom;
	private boolean franchissable;
    
    /**
     * Constructeur de TypeTerrain
     * @param id L'identifiant du type de terrain
     * @param im Le nom d'image correspondant au type de terrain
     * @param n Le nom du type de terrain
     * @param franchiss Le boolean indiquant si le type de terrain sera accessible pour l'unite
     */
	public TypeTerrain(String id, String im, String n , boolean franchiss){
		this.id = id;
		this.image = im;
		this.nom = n;
		franchissable = franchiss;
	}

    /** 
     * @return l'identifiant du type de terrain courant
     */
	public String id() {
		return this.id;
	}
    /**
     * @return le nom d'image du type de terrain courant
     */
	public String image() {
		return this.image;
	}
    /**
     * @return le nom du type de terrain courant
     */
	public String nom() {
		return this.nom;
	}
    /**
     * @return le boolean indiquant si le type de terrain courant est franchissable
     */
	public boolean getFranchissable(){
		return franchissable;
	}
	/**
     * @return la chaine de caracteres correspondant a l'identifiant du type de terrain courant
     */
	public String toString(){
		return id;
	}
	
	
}
