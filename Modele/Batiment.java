package Modele;

import java.util.ArrayList;

/**
 * Batiment est la classe implémentant les bâtiments utilisés par les joueurs
 * Elle hérite de la classe Unite
 * @see Unite
 */
public abstract class Batiment extends Unite{

	/**
	 * plateau correspond au plateau de jeu
	 */
	protected Plateau plateau;
	
	/**
	 * niveau correspond au niveau du bâtiment
	 */
	protected int niveau;
	
	/**
	 * position est une liste de tableaux de case correspondant aux places occupées par le batiment
	 */
	protected ArrayList<Case> position = new ArrayList<Case>();
	
	/**
	 * tailleBat correspond à la largeur (en nombre de cases) du bâtiment
	 */
	protected int tailleBat;
	
	/**
	 * productionParTour est un entier égal au nombre de ressources produites chaque tour dans un certain bâtiment
	 */
	protected int productionParTour;
	
	/**
	 * Constructeur de Batiment
	 * @param pl le plateau de jeu
	 * @param p un entier égal au nombre de points de vie du bâtiment, sa résistance
	 * @param j le joueur propriétaire du bâtiment
	 * @param nivNece le niveau que le château doit nécessairement avoir atteint pour pouvoir construire le Batiment
	 * @param tailleB la taille du bâtiment
	 * @param produc un entier égal au nombre de ressources produites chaque tour dans un certain bâtiment
	 */
	public Batiment(Plateau pl, int p, Joueur j, int nivNece,  int tailleB, int produc) {
		super(p, nivNece, j);
		tailleBat = tailleB;
		plateau = pl;
		niveau = 1;
		productionParTour = produc;
		pdv = pdvMax;
	}
	
	abstract void augmenterNiveau();
	
	/**
	 * Ajoute à la liste des cases occupées par le batiment les (tailleBat)*(tailleBat) cases,
	 * dont celle située sur le coin supérieur gauche est représentée par la coordonnée passée en argument
	 * @param coord les coordonnées de la case occupée par le coin supérieur gauche du batiment 
	 * @see Plateau#getCase(int, int)
	 * @see Case#setOccupant(Unite)
	 */
	public void setPosition(Coordonnee coord){
		int x = coord.x;
		int y = coord.y;
		Case c;
		for (int i=0;i<tailleBat;i++){
			for (int j=0;j<tailleBat;j++){
				c = plateau.getCase(x + i , y + j);
				c.setOccupant(this);
				position.add(c);
			}
		}
	}
	
	/**
	 * Retourne le 1er élément de la liste des cases occupées par le batiment, c'est-à-dire la Case située sur le coin supérieur gauche du batiment   
	 * @return la "premiere" case occupée par le batiment
	 */
	public Case getPosition(){
		return position.get(0);
	}
	
	/**
	 * Retourne la liste de cases occupées par le batiment
	 * @return la liste des cases occupées par le batiment
	 */
	public ArrayList<Case> getPositions(){
		return position;
	}

	/**
	 * Enlève le bâtiment lorsqu'il est éliminé
	 */
	public void meurt() {
		for (Case c : position) c.setOccupant(null);
		joueur.enleverUnite(this);
	}
	
	/**
	 * Retourne le niveau du bâtiment
	 * @return le niveau du bâtiment
	 */
	public int getNiveau(){
		return niveau;
	}
	
	/**
	 * Retourne la taille du bâtiment (le nombre de cases qu'il occupe en largeur)
	 * @return int
	 */
	public int getTailleB(){
		return tailleBat;
	}
	
	/**
	 * Retourne le nombre de ressources produites par le bâtiment à chaque tour 
	 * @return int
	 */
	public int getProduction(){
		return productionParTour;
	}

}
