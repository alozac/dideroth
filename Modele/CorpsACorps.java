package Modele;

/**
 * CorpsACorps est la classe implémentant les unités combattant au corps à corps,
 * c'est-à-dire les unités devant se déplacer à côté de leur cible avant d'attaquer
 * Elle hérite de la classe UniteCombat
 * @see UniteCombat 
 */
public class CorpsACorps extends UniteCombat {
	
	/**
	 * Constructeur de CorpsACorps 
	 * @param pv l'entier égal au nombre de points de vie de l'unité
   * @param pd l'entier égal au nombre de points de dégâts de l'unité
   * @param nbC l'entier égal au nombre de coups que l'unité peut porter
   * @param agi le nombre décimal correspondant au taux d'esquive de l'unité
   * @param porteeD l'entier correspondant à la portée de déplacement de l'unité
   * @param nivNece l'entier correspondant au niveau que le château doit nécessairement avoir atteint pour que le joueur puisse recruter l'unité
   * @param j le joueur auquel l'unité appartient
	 */
	public CorpsACorps(int pv, int pd, int nbC, double agi, int porteeD, int nivNece, Joueur j){ 
		super (pv,pd,nbC,agi,porteeD, porteeD+1, nivNece, j);
	}
	
}
