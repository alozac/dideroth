 /**
  * JeuVS implémente le mode de jeu "Koueur versus Joueur", dans lequel l'objectif de chaque joueurs est de détruire le chateau du second joueur
 * @see Jeu
 */
package Modele;

public class JeuVS extends Jeu {
    /**
     * Construit un jeu avec un plateau et les deux joueurs
     * @param chemin Une String correspondant au chemin absolu pour acceder aux fichiers recherchés
     * @param typeCarte Une String indiquant quel type de carte sera utilisee pour cette partie de jeu
     * @param typeTribuJ1 Une String correspondant au type de tribu choisie pour le joueur 1
     * @param typeTribuJ2 Une String correspondant au type de tribu choisie pour le joueur 2
     */
	public JeuVS(String chemin, String typeCarte, String typeTribuJ1, String typeTribuJ2) {
		plateau = new Plateau(100, chemin, typeCarte);
		getJoueurs().add(new Joueur(plateau, TribuFactory.creerTribu(typeTribuJ1)));
		getJoueurs().add(new Joueur(plateau, TribuFactory.creerTribu(typeTribuJ2)));
		courant = getJoueurs().get(0);
	}	
    
    /**
     * Indique si la partie est terminee ou non
     * Si le chateau d'un de joueur a perdu tous ses points de vie, son adversaire gagne la partie
     * @return true si la partie est terminee et false sinon
     */
	@Override
	public boolean partieTerminee() {
		if (getJoueurs().get(0).getChateau().pdv <= 0) gagnant=getJoueurs().get(1);
		if (getJoueurs().get(1).getChateau().pdv <= 0) gagnant=getJoueurs().get(0);
		if (gagnant != null) return true;
		return false;
	}

}
