package Modele;
/** 
 * UniteVolante est une classe heritee de CorpsACorps, elle repr�sente les unit�s qui peuvent franchir n'importe quelle case (m�me celle cat�goris�e infranchissable)
 */
public class UniteVolante extends CorpsACorps {

	/**
	 * Construit une unite volante, en initialisant ses caracteristiques
	 * @param j le joueur auquel elle appartient
     * @see Unite#remplirCout(int, int, int)
	 */
	public UniteVolante(Joueur j) {
    	//pv, pdd, nbCoups, agi, porteeD, nivNece, joueur
		super(80, 15, 1, 0.3, 10, 3, j);
		remplirCout(550,10,10);
	}
	
    /** 
     * @return La chaine de caracteres permettant de d�crire l'unite volante
     */
	public String toString(){
		return "Dragon";
	}

}
