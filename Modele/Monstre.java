package Modele;
/**
 * Monstre est la classe heritee de CorpsACorps
 */
public class Monstre extends CorpsACorps {
	
    /** 
     * Construit un monstre en utilisant le constructeur de la classe mere avec ses caracteristiques passees en argument
     */
	public Monstre() {
		super(10, 10, 2,0 , 3, 0, null);
		pdv = pdvMax;
	}
	/**
     * Retourne la representation textuelle d'un Monstre
     * @return la representation textuelle d'un Monstre
     */
    public String toString(){
		return "Monstre";
	}
    
    /**
     * Remet l'occupant de la case correspondant a la position du monstre a null lorsque celui-ci meurt
     */
    public void meurt(){
		position.setOccupant(null);
    }
    
	public void ajouteBonusDeTribu(){}

}
