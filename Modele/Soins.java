package Modele;
/**
 * Soins est une classe heritee de Combat. Elle permet a certaines unit�s de soigner ses allies.
 */
public class Soins extends Combat {
	LanceurSorts allie;
	Unite cible;
	/** 
     * Construit des Soins avec les parametres passes en argument
     * @param all Le lanceur de sorts qui est allie � la cible 
     * @param cible L'unite qui sera soignee
     */
	public Soins(LanceurSorts all, Unite cible) {
		super(all);
		allie = all;
		this.cible = cible;
	}
    /** 
     * Soigne la cible par un lanceur des sorts et remplit le rapport de combat
     * @see LanceurSorts#soigner(Unite)
     */
	@Override
	void effectuer_combat() {
		allie.soigner(cible);
		rapport += uniteHTML(cible, coulAll) + " a �t� soign�(e) de <b>"+ allie.getSoin()+"</b>.<br><br>";
	}

}
