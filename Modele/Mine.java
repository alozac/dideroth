package Modele;

/**
 * Mine est une classe heritant Batiment. Une Mine produit du minerai a chaque tour du joueur a qui elle appartient
 */
public class Mine extends Batiment {
    /**
     * Construit une mine 
     * @param j Le joueur auquel elle appartient
     * @param plateau Le plateau du jeu sur lequel elle sera placee
     */
	public Mine(Joueur j, Plateau plateau) {
		super(plateau, 30, j,2, 2, 1);
		for (int i = 1; i < j.getChateau().niveau; i++){
			augmenterNiveau();
		}
		remplirCout(500,5,5);
	}
	/** 
     * Augmente le niveau d'une mine
     * @see #setProduc(int)
     */
	public void augmenterNiveau(){
		niveau++; 
		setProduc(1);
	}
	
    /**
     * Rajoute des ressources provenant de la mine au joueur courant a la fin de tour
     * @see Joueur#ajouterRess(String, int)
     */
	public void finDeTour(){
		joueur.ajouterRess("Minerai", productionParTour);
	}
	/**
     * Modifie la production par tour en l'augmentant par un nombre donne en parametre
     * @param i Le nombre avec lequel on augmentera la production par tour
     */
	public void setProduc (int i){
		productionParTour+=i;
	}
	/**
     * @return Un String indiquant qu'il s'agit d'une mine
     */
	public String toString(){
		return "Mine";
	}
    /**
     * @return Un String correspondant au nom de l'image d'une mine
     */
	@Override
	public String identImage() {
		return "mine";
	}
}
