package Modele;

import java.util.HashMap;

/**
 * Unite est une classe repr�sentant les unites
 */

public abstract class Unite {
    /** 
     * Le nombre de points de vie de l'unite courante
     */
	protected int pdv;
    /**
     * Le nombre maximal de points de vie de l'unite courante
     */
	protected int pdvMax;
    /**
     * Le niveau necessaire du chateau
     */
	protected int nivChateauNecessaire;
    /**
     * Le joueur auquel elle appartient
     */
	protected Joueur joueur;
    /**
     * L'emplacement de l'unite sur le plateau de jeu    
     */
	protected Case position;
    /**
     * Le hashmap des noms et des couts correspondant de ressources 
     */
	protected HashMap<String, Integer> cout = new HashMap<String, Integer>();
	
    /** Construit une unite avec les parametres donnees en argument 
     * @param vie Son nombre de points de vie
     * @param nivNece Le niveau necessaire du chateau pour pouvoir acheter l'unite 
     * @param j joueur auquel elle va appartenir 
     */
	public Unite(int vie, int nivNece, Joueur j){
		joueur=j;
		pdvMax=vie;
		nivChateauNecessaire = nivNece;
	}
	/** Remplit un hashmap avec les noms et les couts correspondant des ressources
     * @param or Le cout de l'or
     * @param bois Le cout du bois
     * @param mine Le cout du minerai
     */
	public void remplirCout(int or, int bois, int mine) {
		cout.put("Or", or);
		cout.put("Bois", bois);
		cout.put("Minerai", mine);
	}
	
    /**
     * Modifie la position de l'unite
     * @param empl La case correspondant a la nouvelle position
     */
	public void setPosition(Case empl) {
		position=empl;
	}
	
	abstract void finDeTour();
	
	abstract void meurt();
	
	public abstract String identImage();
	
    /** 
     * Ajoute le bonus correspondant � la tribu du joueur courant
     * @see Tribu#bonusDeTribu(Unite)
     */
    public void ajouteBonusDeTribu(){
    	joueur.getTribu().bonusDeTribu(this);
    }
    
    /**
     * Permet de savoir le niveau du chateau necessaire pour acheter l'unite
     * @return Le niveau de chateau necessaire
     */
	public int getNivChateauNece(){
		return nivChateauNecessaire;
	}
	
    /** Indique si l'unite courante est morte
     * @return le boolean valant true si le nombre de points de vie est inferieure ou egal a 0
     */
	public boolean estMort(){
    	return pdv<=0;
    }
    /** Retourne le nombre de points de vie de l'unite courante
     * @return pdv
     */
    public int getPdv(){
    	return pdv;
    }
    /** Retourne la chaine de caracteres correspondant a la relation entre pdv et pdvMax
     * @return pdv + "/" + pdvMax
     */
    public String getRatioVie(){
    	return pdv + "/" + pdvMax;
    }
    
    /** Retourne la case correspondant a la position actuelle de l'unite sur le plateau de jeu
     * @return position 
     */
	public Case getPosition() {
		return position;
	}
	/** Retourne le cout des ressources pour l'unite courante
     * @return cout
     */
	public HashMap<String, Integer> getCout(){
		return cout;
	}
    /** Retourne le joueur auquel appartient l'unite courante 
     * @return joueur 
     */
	public Joueur getJoueur() {
		return joueur;
	}
}
