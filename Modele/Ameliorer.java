package Modele;

/**
 * Ameliorer est la classe permettant d'apporter des améliorations aux propriétés du joueur
 * (augmenter le niveau des bâtiments et certaines de leurs caractéristiques (production par tour par exemple))
 * Elle hérite de la classe Action
 * @see Action
 */
public class Ameliorer extends Action {

	/**
	 * bat correspond au château du joueur
	 */
	Chateau bat;
	
	/**
	 * Constructeur d'Ameliorer
	 * @param j le joueur ayant la main
	 * @see Joueur#getChateau()
	 */
	public Ameliorer(Joueur j){
		super(j, null);
		this.bat = j.getChateau();
	}
	
	/**
	 * Permet d'augmenter le niveau des bâtiments du joueur
	 * @see Batiment#getCout()
	 * @see Joueur#getBatiments()
	 * @see Batiment#augmenterNiveau()
	 * @see Joueur#setPa(int)
	 */
	@Override
	public void effectuer_action() {
		j.depenserRess(1, bat.getCout());
		for (Batiment b : j.getBatiments())
			b.augmenterNiveau();
		j.setPa(1);
	}
	
	/**
	 * Détermine si l'amélioration est possible
	 * @return  true si les conditions suivantes sont toutes satisfaites :
	 * le château du joueur n'est pas déjà en cours d'amélioration (en construction)
	 * le joueur a les ressources en or nécessaires pour effectuer l'amélioration
	 * le joueur a encore au moins 1 point d'action pour effectuer l'amélioration
	 * Retourne false sinon
	 */
	public boolean amelPossible() {
		return !bat.enConstruction && j.maxDepensePossible(bat.getCout()) > 0  && j.getPa() > 0;
	}
	
}
