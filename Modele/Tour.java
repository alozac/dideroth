package Modele;

import java.util.HashSet;
import java.util.Set;

/** 
 * Tour est une classe heritee de Batiment. Elle représente les tours que peuvent acquérir le joueur
 */
public class Tour extends Batiment {

	private int pdd;
	private int porteeA;
	private HashSet<Case> casesAPortee= new HashSet<Case>() ;
    /**
     * Construit une tour avec les parametres passes en argument et ses caracteristiques
     * @param j Le joueur auquel elle appartiendra 
     * @param p Le plateau de jeu sur lequel elle sera placée
     */
	public Tour(Joueur j, Plateau p) {
		super(p, 50, j, 3, 1, 0);
		pdd = 2;
		porteeA = 3;
		remplirCout(1000,10,10);
	}
	/**
     * Augmente le niveau d'une tour et son nombre de points de degat
     */
	public void augmenterNiveau(){
		niveau ++;
		pdd++;
	}
	/**
     * Permet de placer la tour 
     * @param coord Les coordonnees sur lesquelles se trouvera la tour
     * @see setCasesAPortee
     */
	public void setPosition(Coordonnee coord){
		super.setPosition(coord);
		setCasesAPortee(porteeA, new HashSet<Case>());
	}
	
    /** 
     * Attaque le cible en lui enlevent ses points de vie
     * @param cible L'unite qui sera attaquee
     * @return true si l'unite est morte et false sinon
     * @see Unite#meurt()
     * @see Unite#estMort()
     */
     public boolean attaquer(Unite cible) {
		cible.pdv -= pdd;
		if (cible.estMort()) {
			cible.meurt();
			return true;
		}
		return false;
	}
	
	/**
	 * Est une fonction récursive qui permet de déterminer les cases qui sont à portée de la tour.
	 * @param portee représente la portée de la tour
	 * @param dernAjout correspond aux cases ajoutees dans la liste de cases à portee dans le dernier appel de cette case a portee
	 */
	public void setCasesAPortee(int portee, HashSet<Case> dernAjout) {
		if (porteeA == portee)
			dernAjout.add(getPosition());
		HashSet<Case> tmp = new HashSet<Case>();
		HashSet<Case> nouvAjout = new HashSet<Case>();
		for (Case c : dernAjout)
			tmp.addAll(definirCasesAdja(c));
		for (Case c : tmp) {
			casesAPortee.add(c);
			nouvAjout.add(c);
		}
		if (portee - 1 != 0) setCasesAPortee(portee - 1, nouvAjout);
		else casesAPortee.remove(getPosition());
	}

	/**
	 * Permet de definir les cases adjacentes à la case c
	 * @param c la case dont on veut definir les cases adjacentes
	 * @return
	 */
	private Set<Case> definirCasesAdja(Case c) {
		Set<Case> list = new HashSet<Case>();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (Math.abs(i + j) == 1) {
					Case ca = plateau.getCase(c.coord.x + i, c.coord.y + j);
					if (ca != null) list.add(ca);
				}
			}
		}
		return list;
	}
	/**
	 * Permet d'obtenir les cases à portée de la tour
	 * @return une HasSet de case, qui représente les cases à portée de la tour
	 */
	public HashSet<Case> getCasesAPortee(){
		return casesAPortee;
	}
	
    /** 
     * @return La portee d'attaque de la tour
     */
	public int getPorteeA(){
		return porteeA;
	}
    /**
     * @return Le nombre de points de degat
     */
    public int getPdd(){
    	return pdd;
    }
    
    /** 
     * @return La chaine de caracteres indiquant qu'il s'agit d'une tour
     */
    @Override
	public String toString() {
		return "Tour";
	}
    
    /**
     * @return La chaine de caracteres correspondant a l'identifiant d'image d'une tour
     */
	@Override
	public String identImage() {
		return "tour";
	}

	@Override
	void finDeTour() {
	}
	
	

}
