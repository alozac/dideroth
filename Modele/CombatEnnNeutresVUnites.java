package Modele;

/**
 * CombatEnnNeutresVUnites est la classe implémentant le combat entre monstres et unités de combat dans le mode Joueur versus Ennemi
 * Elle hérite de la classe Combat
 * @see Combat
 */ 
public class CombatEnnNeutresVUnites extends Combat {

	Monstre attaquant;
	Unite cible;
	boolean riposteEnnemi;
	
	/**
	 * 3 entiers correspondant au nombre de victimes respectivement parmi les bleus (joueur 1), parmi les rouges (joueur 2), parmi les monstres
	 */
	static int nbVicB, nbVicR, nbVicM;
	
	/**
   * Constructeur de CombatEnnNeutresVUnites
   * @param att correspondant au monstre attaquant
   * @param cib correspondant à l'unité cible 	
	 */
	public CombatEnnNeutresVUnites(Monstre att, Unite cib) {
		super();
		attaquant = att;
		cible = cib;
		riposteEnnemi = cible instanceof CorpsACorps;
	}

	/**
	 * Implémente le combat entre monstre et unité  
	 */
	@Override
	void effectuer_combat() {
		boolean cibleUnitCbt = cible instanceof UniteCombat;
		int nbCAll = attaquant.nbCoups;
		int nbCEnn = cibleUnitCbt ?((UniteCombat)cible).nbCoups : 0;
		while (nbCAll > 0 || (nbCEnn > 0 && riposteEnnemi)){
			if (nbCAll>0){
				attaquant.attaquer(cible);
				if (cible.estMort()) 	{
					if (cible.getJoueur().getNum()==1)nbVicB++;
					else nbVicR++;
					return;
				}
				nbCAll--;
			}
			if (riposteEnnemi && nbCEnn >0){
				((UniteCombat) cible).attaquer(attaquant);
				if (attaquant.estMort()) {
					nbVicM++;
					return;
				}
				nbCEnn--;
			}
		}
	}
	
	/**
	 * Renvoie le rapport rédigé
	 * @return une chaîne résumant le nombre de victimes faites dans chacun des camps
	 */
	public static String ecrireRapport(){
		String rap = "";
		if (nbVicB!=0 || nbVicR!=0)
			rap += "<b>" +nbVicB + "</b> unité(s) <font color=blue>Bleue(s)</font> et <b>" + 
				   nbVicR + "</b> unité(s) <font color=red>Rouges</font> tuée(s) par l'ennemi.<br>";
		if (nbVicM != 0) rap+="Nombre d'ennemis tués: "+nbVicM+".<br>";
		nbVicB = 0;
		nbVicR = 0;
		nbVicM = 0;
		return rap + "<br>";	
	}

}
