package Modele;
/** 
 * Nain est une classe heritee de Tribu. Les Nains ont un avantage au niveau des points de vie.
 */
public class Nain extends Tribu {
        /** 
         * Constructeur de la calsse Nain.
         * Utilise le constucteur de la classe mere
         */
		public Nain(){
			super("Nain", 0.4, 0);
		}
        /** 
         * Ajoute des points de vie supplementaires a une unite donnee en argument
         * @param u L'unite a laquelle on va augmenter le nombre maximal de points de vie
         */
		@Override
		void ajoutePdvSuppl(Unite u) {
			int nouvPv = (int) (u.pdvMax + u.pdvMax * bonusVie);
			u.pdvMax = nouvPv;
			u.pdv = u.pdvMax;
		}

		@Override
		void ajouteAttaqueSuppl(UniteCombat u) {
			// TODO Auto-generated method stub
			
		}
}
