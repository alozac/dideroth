package Modele;

/**
 * La classe TypeTerrainFactory permet de construire un type de terrain � partir d'une string
 */
public class TypeTerrainFactory {
    /**
     * Permet de creer un nouveau type de terrain
     * @param type L'identifiant du type terrain qu'on veut creer
     * @param image Le nom d'image correspondant a ce type de terrain
     * @param nom Le nom de type de terrain
     * @param franchiss valant true ou false selon la franchissabilite du type de terrain
     * @return Un nouveau type de terrain avec les parametres qu'on vient d'ecrire
     */
	public static TypeTerrain creerTerrain(String type, String image, String nom, String franchiss){
		boolean franchissable = franchiss.equals("true")?true:false;
		switch(nom){
		case "montagne":
		case "m":
			return new Montagne( type, image, nom, franchissable);
		case "desert":
		case "d":
			return new Desert( type, image, nom, franchissable);
		case "foret":
		case "f":
			return new Foret(type, image, nom, franchissable);
		case "lac":
		case "l":
			return new Lac(type, image, nom, franchissable);
		}
		return null;
	}
}
