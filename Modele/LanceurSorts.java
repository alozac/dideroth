package Modele;

/** 
 * Une unite instanciant LanceurSorts peut, en plus d'attaquer les ennemis, soigner ses alliés
 * LanceurSorts est une classe heritant UniteCombat
 */
public class LanceurSorts extends UniteCombat {
    /**
     * Le nombre avec laquelle on va soigner les unites
     */
	protected int soin;
	
    /**
     * Construit un lanceur de sorts avec les parametres donnees en argument
     * @param pv le nombre de points de vie de l'unite
	 * @param pd le nombre de points de degat de l'unite
	 * @param nbC le nombre de coups portes par l'unite au cours d'une attaque
	 * @param agi le taux d'esquive de l'unite
	 * @param porteeD la portee de deplacement de l'unite
	 * @param porteeA la portee d'attaque (et de soin) de l'unite
	 * @param nivNece le niveau que doit avoir au minimum le chateau du joueur pour pouvoir acheter ce type d'unite
	 * @param j le joueur a qui appartient l'unite
     * @param soin le nombre de points de vie restitues a l'unite qu'un lanceur de sort soigne
     * @see #UniteCombat la signification les autres parametres est explique la-bas
     */
	public LanceurSorts(int pv, int pd, int nbC, double agi, int porteeD, int porteeA, int nivNece, Joueur j, int soin){ 
		super (pv,pd,nbC,agi,porteeD,porteeA, nivNece, j);
		this.soin = soin;
	}
	/**
     * Soigne l'unite donnee en argument en lui augmentant son nombre de points de vie
     * @param cible L'unite laquelle on soignera
     */
    public void soigner (Unite cible){
	    if (soin+cible.pdv > cible.pdvMax) cible.pdv = cible.pdvMax;
	    else cible.pdv += soin;
	}
	/**
	 * Retourne le nombre de points de vie restitues par cette unite lorsqu'elle en soigne une autre
     * @return le nombre correspondant a la valeur du soin
     */
	public int getSoin(){
		return soin;
	}
    /**
     * Retourne une chaine indiquant les informations relatives a l'attaque et au soin de l'unite
     * @return Un String correspondant a un rationnel avec la valeur du soin entre les parentheses
     * @see UniteCombat#getRatioAttaque()
     */
	public String getRatioAttaque(){
		return super.getRatioAttaque() + " ( " + soin + " )";
	}
}
