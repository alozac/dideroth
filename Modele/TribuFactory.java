package Modele;
/** 
 * TribuFactory est la classe permettant de construire de nouvelles tribus
 */
public class TribuFactory {
	
    /**
     * Construit la nouvelle tribu en prenant son nom en argument 
     * @param trib La chaine de caracteres correspondant au nom de la tribu qui sera creee
     * @return la nouvelle tribu 
     */
	public static Tribu creerTribu(String trib){
		switch(trib){
		case "Humain":
			return new Humain();
		case "Elfe":
			return new Elfe();
		case "Nain":
			return new Nain();
		}
		return null;
	}

}
