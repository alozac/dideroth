package Modele;
import java.util.ArrayList;
import java.util.HashMap;

/** 
 * La classe Joueur permet de creer un nouveau joueur 
 */
public class Joueur {
	private Plateau plateau;
	private ArrayList<UniteCombat> unites = new ArrayList<UniteCombat>();
	private ArrayList<Batiment> batiments = new ArrayList<Batiment>();
	private int uniteCourante = 0;
	private HashMap<String, Integer> ressources;
	private Tribu tribu;
	private int point_action = 10;
	private int numJoueur;
	private String coulJoueur;
	public static int nbJoueurs = 0;
   
    /**
     * Construit un joueur avec les parametres passes en argument et en creant egalement un Chevalier et un Archer présents dès le début de la partie
     * @param p Le plateau de jeu lie au joueur en construction
     * @param trib La tribu choisie pour ce joueur
     */
   public Joueur (Plateau p, Tribu trib){
		plateau = p;
		tribu = trib;
		nbJoueurs++;
		numJoueur = nbJoueurs;
		coulJoueur = numJoueur == 1 ? "Bleu" : "Rouge";
		ressources = new HashMap<String, Integer>();
		ressources.put("Or", 5000);
		ressources.put("Bois", 10);
		ressources.put("Minerai", 10);
		batiments.add(new Chateau(this, p, affecterChateau()));
		ajouteUnite(new Chevalier(this));
		ajouteUnite(new Archer(this));
	}
    /**
     * Permet de déterminer l'emplacement du chateau pour le joueur
     * @return Les coordonnees d'emplacement du chateau
     */
	public Coordonnee affecterChateau(){
		int taille = plateau.taille;
		if (numJoueur == 1) return new Coordonnee(0,taille%2==0?taille/2-1:taille/2);
		return new Coordonnee(taille-2 , taille%2==0?taille/2-1:taille/2);
	}
	
    /**
     * Permet d'effectuer les actions liées à la fin d'un tour du joueur
     * @return Le rapport de fin de tour
     * @see UniteCombat#finDeTour
     * @see Batiment#finDeTour
     * @see CombatTour
     */
	public String finDeTour() {
		String rap = "";
		point_action = 10;
		for (UniteCombat u : unites)
			u.finDeTour();
		for (Batiment b : batiments){
			b.finDeTour();
			if (b instanceof Tour) {
				CombatTour c = new CombatTour((Tour) b);
				c.effectuer_combat();
				rap += c.rapport;
			}
		}
		return rap;
	}
	/**
     * Ajoute une unite de combat
     * @param u L'unite de combat qui sera ajoutee a la liste de toutes les unites du joueur
     * @see Chateau#ajouteUnite(UniteCombat)
     */
	public void ajouteUnite(UniteCombat u) {
		unites.add(u);
		getChateau().ajouteUnite(u);
	}
    /**
     * Enleve une unite donnee en argument soit de la liste des unites soit de la liste des batiments
     * @param u L'Unite qui sera enlevee de joueur
     */
	public void enleverUnite(Unite u) {
		if (u instanceof UniteCombat)
			unites.remove(u);
		else
			batiments.remove(u);
	}
    /**
     * Ajoute des ressources au hashmap des ressources du joueur courant
     * @param typeRess Le type des ressources
     * @param nb La quantite des ressources 
     */
	public void ajouterRess(String typeRess, Integer nb) {
		ressources.put(typeRess, ressources.get(typeRess) + nb);
	}
    /**
     * Depense des ressources du joueur courant, enleve la quantite donnee du hashmap des ressources
     * @param typeRess Le type des ressources a enlever
     * @param nb La quantite des ressources a enlever
     */
	public void depenserRess(String typeRess, Integer nb) {
		ressources.put(typeRess, ressources.get(typeRess) - nb);
	}
    
    /**
     * Depense d'une certaine quantite de toutes les ressources de joueur courant
     * @param qte La quantite de depense
     * @param cout Le hashmap avec les couts des ressources 
     * @see #depenserRess(String, Integer)
     */
	public void depenserRess(Integer qte, HashMap<String, Integer> cout){
		depenserRess("Or", cout.get("Or") * qte);
		depenserRess("Bois", cout.get("Bois") * qte);
		depenserRess("Minerai", cout.get("Minerai") * qte);
	}
	/**
     * @return Le minimum entre les trois maximums de depenses possibles correspondant aux trois types de ressources 
     * @see #maxDepPossible(String, int)
     */
	public int maxDepensePossible(HashMap<String, Integer> cout) {
		return Math.min(maxDepPossible("Or", cout.get("Or")),
			   Math.min(maxDepPossible("Bois", cout.get("Bois")), maxDepPossible("Minerai", cout.get("Minerai"))));
	}
	/**
     * Calcule la depense maximale possible pour un certain type de ressources
     * @param typeRess Le type de ressources pour lequel on calculera la depense maximale
     * @param cout Le cout de ce type de ressources 
     * @return La depense maximale
     */
	public int maxDepPossible(String typeRess, int cout) {
		int maxDep = 1;
		int qteDispo = ressources.get(typeRess);
		while(true){
			if (cout * maxDep <= qteDispo) maxDep++ ;
			else return maxDep - 1;
		}
	}
	
    /**
     * Indique si le joueur courant possede le batiment donne en argument
     * @param occupant Le Batiment en question
     * @return Le boolean indiquant si le batiment recherche appartient au joueur ou pas
     */
	public boolean possedeBatiment(Batiment occupant) {
		for (Batiment b : batiments)
			if (b == occupant) return true;
		return false;
	}
    /**
     *Retourne le chateau du joueur
     * @return Le chateau du joueur
     */
	public Chateau getChateau() {
		return (Chateau) batiments.get(0);
	}
    /**
     * Retourne le nombre de points d'actions restants au joueur durant le tour courant
     * @return Les points d'action du joueur
     */
	public int getPa() {
		return point_action;
	}
    /**
     * Modifie le nombre de points d'action du joueur
     * @param n Le nombre de points d'action qui sera enleve
     */
	public void setPa(int n) {
		point_action -= n;
	}
    
    /**
     * Retourne l'index, dans la liste des unités, de la premiere unité à ne pas encore avoir attaqué pendant le tour courant
     * @return L'identifiant de l'unite courante
     */
	public int getUniteCourante() {
		return uniteCourante;
	}
    /**
     * @param n L'identifiant de la nouvelle unite courante
     */
	public void setUniteCourante(int n) {
		uniteCourante = n;
	}
    /**
     * Retourne la liste des unites de combat du joueur
     * @return La liste des unites du joueur
     */
	public ArrayList<UniteCombat> getUnites() {
		return unites;
	}
    /**
     * Retourne la liste des batiments du joueur
     * @return La liste des batiments du joueur
     */
	public ArrayList<Batiment> getBatiments() {
		return batiments;
	}
    /**
     * Retourne la couleur du joueur
     * @return La couleur du joueur courant sous la forme d'une chaine de caracteres
     */
	public String getCoulJoueur() {
		return coulJoueur;
	}
    /**
     * Retourne le numero du joueur
     * @return Le numero du joueur courant
     */
	public int getNum() {
		return numJoueur;
	}
    /**
     * Permet d'obtenir le quantité restante du type de ressource donné en parametre
     * @param typeRess Le type des ressources dont on cherche la quantite
     * @return La quantite des ressources
     */
	public Integer getRess(String typeRess) {
		return ressources.get(typeRess);
	}
    /**
     * Retourne le Tribu du joueur
     * @return Le tribu du joueur
     */
	public Tribu getTribu(){
		return tribu;
	}
}
