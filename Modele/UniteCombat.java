package Modele;
/**
 * Repr�sente les unit�s pouvant combattre
 */
public abstract class UniteCombat extends Unite {
    protected int pdd;
    protected int nbCoups;
    protected double agilite;
    protected int porteeDeplacement;
    protected int porteeAttaque;
    protected boolean aAttaque = false;
    
    /**
     * Construit une unite de combat avec les caract�ristiques donnees en argument
   	 * @param pv son nombre de points de vie
	 * @param pd son nombre de points de degat
	 * @param nbC le nombre de coup qu'elle porte lors d'une attaque
	 * @param agi son taux d'agilite (compris entre 0 et 1, avec 0 aucune esquive possible)
	 * @param porteeD le nombre de cases qu'elle peut traverser lors d'un mouvement
	 * @param porteeA la distance maximale (en case) a laquelle l'unit� peut attaquer
	 * @param j le joueur auquel elle appartient
     */
    public UniteCombat(int pv, int pd, int nbC, double agi, int porteeD, int porteeA, int nivNece, Joueur j){
    	super (pv, nivNece, j);
		pdd=pd;
		nbCoups = nbC;
		agilite = agi;
		porteeDeplacement=porteeD;
		porteeAttaque = porteeA;
		ajouteBonusDeTribu();
    }	
    
    /** Attaque l'unite passee en argument
     * @param cible L'unite qu'on va attaquer
     * @return Le boolean indiquant si l'attaque a eu lieu
     */
    public boolean attaquer(Unite cible){
    	boolean att = true;
    	if (cible instanceof UniteCombat) att = Math.random() > ((UniteCombat)cible).agilite;
    	if (att) cible.pdv-=this.pdd;
    	if (!(cible instanceof Chateau) && cible.estMort()) 
    		cible.meurt();
    	return att;
    }
    
    /** 
     * Tue l'unite courante, l'enleve du joueur auquel elle appartenait et remet l'occupant de la case ou elle se trouvait a null
     * @see Joueur#enleverUnite
     * @see Case#setOccupant
     */
    public void meurt() {
		joueur.enleverUnite(this);
		position.setOccupant(null);
	}
    
    /**
     * Met l'attribut aAttaque a false
     */
	public void finDeTour() {
		aAttaque = false;	
	}
    
    /**
     * Permet de savoir le nombre de point de d�gat de l'unit�
     * @return Le nombre de points de degat
     */
    public int getPdd(){
    	return pdd;
    }
    
    /**
     * Modifie le nombre de points de degat
     * @param pd le nouvel nombre de points de degat
     */
	public void setPdd(int pd) {
		pdd = pd;
	}
    
    /**
     * Permet de savoir le pourcentage de chance qu'a l'unite d'esquiver
     * @return Le taux d'agilite d'unite courant
     */
    public double getAgilite(){
    	return agilite;
    }
    
    /**
     * Permet de savoir combien de fois l'unite a attaque et a combien s'eleve ses points de degats
     * @return Le rapport entre nombre de coups et le nombre de points de degat
     */
    public String getRatioAttaque(){
    	return nbCoups + " * " + pdd;
    }
    /**
     * Permet de savoir quelles sont les portees d'attaque et de deplacement de l'unite
     * @return Une chaine de caracteres correspondant aux portees de deplacement et d'attaque
     */
	public String getPortees() {
		return porteeDeplacement + " - " + porteeAttaque;
	}
    /**
     * Permet d'obtenir la portee de deplacement de l'unite
     * @return Le nombre correspondant a la portee de deplacement
     */
	public int getPorteeD() {
		return porteeDeplacement;
	}
    /**
     * Permet d'obtenir la portee d'attaque de l'unite
     * @return Le nombre correspondant a la portee d'attaque
     */
	public int getPorteeA() {
		return porteeAttaque;
	}
    /**
     * Permet de savoir si l'unite a attaque ou non
     * @return true si l'unite est prete a l'attaque
     */
	public boolean aAttaque() {
		return aAttaque;
	}
    /**
     * Permet d'obtenir l'image correspondant � l'unite
     * @return Une chaine de caracteres correspondant a l'image de l'unite de combat
     */
	public String identImage(){
		return toString();
	}

}
