package Modele;
import java.util.ArrayList;
import java.util.Observable;

/**
 * Jeu est la classe implémentant le jeu, contenant les joueurs, le plateau de jeu et s'occupant de vérifier si la partie est terminée ou non
 * Elle hérite de la classe Observable
 */
public abstract class Jeu extends Observable {

	protected Plateau plateau;
	protected Joueur gagnant;
	protected Joueur courant;

	/**
	 * joueurs est une liste de joueurs indiquant tous les joueurs participant à la partie
	 */
	private ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
	
	/**
	 * on itèrera numTour à chaque fin de tour, il indique à combien de tours en est la partie
	 */
	public static int numTour = 1;
		
	public abstract boolean partieTerminee();

	/**
	 * Passe la main au joueur suivant
	 */
	public void joueurSuivant() {
		courant = getJoueurs().get(courant.getNum()%getJoueurs().size());
	}
	
	@Override
	public void setChanged() {
		super.setChanged();
	}

	/**
	 * Retourne le plateau de jeu
	 * @return Plateau
	 */
	public Plateau getPlateau() {
		return plateau;
	}
	
	/**
	 * Retourne le joueur courant
	 * @return Joueur
	 */
	public Joueur getCourant(){
		return courant;
	}
	
	/**
	 * Retourne le gagnant s'il existe
	 * @return Joueur
	 */
	public Joueur getGagnant(){
		return gagnant;
	}

	/**
	 * Retourne la liste de joueurs
	 * @return la liste des joueurs
	 */
	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}
}
