package Modele;

/**
 * Chevalier est la classe représentatnt l'unité des chevaliers, combattant au corps à corps
 * Elle hérite de la classe CorpsACorps
 * @see CorpsACorps
 */
public class Chevalier extends CorpsACorps{
		
	/**
	 * Constructeur de Chevalier
	 * @param j le joueur auquel le Chevalier appartient
	 * @see Unite#remplirCout(int, int, int)
	 */
	public Chevalier(Joueur j) {
		// pv, pdd, nbCoups, agi, porteeD, nivNece, joueur
		super(16, 4, 2, 0.3, 7, 1, j);
		remplirCout(125, 5, 5);
	}

	/**
	 * Retourne une chaîne de caractères indiquant le nom de l'unité des
	 * chevaliers
	 * @return String le type de l'unité
	 */
	public String toString() {
		return "Chevalier";
	}
  
}
