package Modele;

/**
 * ADistance est la classe définissant les unités combattant à distance (qui n'ont pas besoin de se déplacer avant d'attaquer)
 * Elle hérite de la classe UniteCombat
 * @see UniteCombat
 */
public class ADistance extends UniteCombat {
  
  /**
   * Constructeur de ADistance
   * @param pv l'entier égal au nombre de points de vie de l'unité
   * @param pd l'entier égal au nombre de points de dégâts de l'unité
   * @param nbC l'entier égal au nombre de coups que l'unité peut porter
   * @param agi le nombre décimal correspondant au taux d'esquive de l'unité
   * @param porteeD l'entier correspondant à la portée de déplacement de l'unité
   * @param porteeA l'entier correspondant à la portée d'attaque de l'unité
   * @param nivNece l'entier correspondant au niveau que le château doit nécessairement avoir atteint pour que le joueur puisse acheter l'unité
   * @param j le joueur auquel l'unité appartient
   */  
	public ADistance(int pv, int pd, int nbC, double agi, int porteeD, int porteeA, int nivNece, Joueur j){ 
		super (pv, pd, nbC, agi, porteeD, porteeA, nivNece, j);
	}

}
