package Modele;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * JeuCoop est la classe implémentant le jeu en mode Joueur versus Ennemi, les deux camps coopèrent pour éliminer les monstres présents sur le terrain
 * Elle hérite de la classe Jeu
 * @see Jeu
 */
public class JeuCoop extends Jeu {
	
	/**
	 * listeMonstres est une liste de tableaux de monstres listant les monstres présents sur le terrain
	 */
	private ArrayList<Monstre> listeMonstres = new ArrayList<Monstre>();
	private int nbTotalMonstres;
	
	/**
	 * Dans ce mode de jeu, le nombre de tours est décidé à l'avance, voilà à quoi correspond l'entier nbToursMax.
	 * Il sera égal à -1 si les joueurs décident d'avoir un nombre de tours illimités
	 */
	private int nbToursMax;

	/**
	 * Constructeur de JeuCoop
	 * @param chemin une chaîne de caractères désignant le chemin absolu pour parvenir au dossier contenant le projet
	 * @param typeCarte une chaîne de caractère indiquant le type d'environnement choisi (désert, forêt ou montagne)
	 * @param typeTribuJ1 une chaîne de caractères correspondant à la tribu choisie par le joueur 1
	 * @param typeTribuJ2 une chaîne de caractères correspondant à la tribu choisie par le joueur 2
	 * @param paramModeCoop un couple d'entiers, le premier représentant le nombre de monstres à placer sur le terrain,
	 * et le second le nombre de tours choisi par les joueurs
	 */
	public JeuCoop(String chemin, String typeCarte, String typeTribuJ1, String typeTribuJ2, int[] paramModeCoop) {
		plateau = new Plateau(100, chemin, typeCarte);
		nbTotalMonstres = paramModeCoop[0];
		nbToursMax = paramModeCoop[1];
		mettreMonstres();
		getJoueurs().add(new Joueur(plateau, TribuFactory.creerTribu(typeTribuJ1)));
		getJoueurs().add(new Joueur(plateau, TribuFactory.creerTribu(typeTribuJ2)));
		courant = getJoueurs().get(0);
	}
	
	/**
	 * Place aléatoirement "nbTotalMonstres" sur le plateau de jeu
	 * @see Case#estLibre()
	 * @see Case#estFranchissable()
	 * @see Case#setOccupant(Unite)
	 */
	public void mettreMonstres(){
		int nbMstresTmp = nbTotalMonstres;
		while (nbMstresTmp != 0){
			int rd = (int)(Math.random() * (plateau.taille - 10) + 5);
			int rd1 = (int)(Math.random() * plateau.taille);
			if(plateau.cases[rd][rd1].estLibre() && plateau.cases[rd][rd1].estFranchissable()){
				Monstre m = new Monstre();
				listeMonstres.add(m);
				plateau.cases[rd][rd1].setOccupant(m);
				nbMstresTmp--;
			}
		}
	}
	
	/**
	 * Permet d'attaquer les monstres et renvoie le rapport de l'attaque
	 * @return String le rapport résumant l'attaque des monstres
	 * @see Unite#estMort()
	 * @see AttaqueEnnNeutres#effectuer_action()
	 */
	public String attaqueEnnemisNeutres() {
		for (Iterator<Monstre> it = listeMonstres.iterator(); it.hasNext();) {
			Monstre m = it.next();
			if (m.estMort())
				it.remove();
		}
		AttaqueEnnNeutres att = new AttaqueEnnNeutres(listeMonstres, plateau);
		att.effectuer_action();
		return att.rapport;
	}

	/**
	 * Détermine si une partie est terminée ou non
	 * @return true si le château de l'un des deux joueurs a été détruit ou si tous les monstres ont été éliminés sans avoir dépassé le nombre de tours imposé,
	 * false sinon 
	 */
	public boolean partieTerminee(){
		for (Joueur j : getJoueurs())
			if (j.getChateau().estMort()) return true; 
		if (nbToursMax != -1)
			return listeMonstres.size() == 0 || numTour > nbToursMax ;
		return listeMonstres.size() == 0;
	}
	
	/**
	 * Renvoie le nombre de monstres restants sur le terrain
	 * @return le nombre de monstres encore présents sur la carte
	 */
	public int getNbMonstres(){
		return listeMonstres.size();
	}
	
	/**
	 * Renvoie la liste de monstres
	 * @return les monstres présents sur le terrain
	 */
	public ArrayList<Monstre> getListeMonstres(){
		return listeMonstres;
	}
	
	/**
	 * Renvoie le nombre total de monstres qu'il y avait au départ sur le terrain
	 * @return le nombre initial de monstres
	 */
	public int getNbTotalMonstres(){
		return nbTotalMonstres;
	}
	
	/**
	 * Renvoie le nombre de tours que l'on peut effectuer au maximum
	 * @return le nombre de tours choisi par les joueurs (est égal à -1 s'ils ont un nombre de tours illimité)
	 */
	public int getNbToursMax(){
		return nbToursMax;
	}
}
