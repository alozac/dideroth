package Modele;
/**
 * Scierie est une classe heritee de Batiment. La Scierie est un batiment permettant d'obtenir la ressource "Bois".
 */
public class Scierie extends Batiment {
    /**
     * Contruit une scierie
     * @param j Le joueur pour lequel la scierie sera creee
     * @param p Le plateau sur lequel la scierie sera placee
     * @see Unite#remplirCout(int, int, int)
     */
	public Scierie(Joueur j, Plateau p) {
		super(p, 30 , j, 2, 2, 1);
		for (int i = 1; i < j.getChateau().niveau; i++){
			augmenterNiveau();
		}
		remplirCout(500,5,5);
	}
    /**
     * Augmente le niveau d'une scierie
     * @see #setProduc(int)
     */
	public void augmenterNiveau(){
		niveau++;
		setProduc(1);	
	}
    /**
     * Rajoute des ressources provenant de la scierie au joueur courant a la fin de tour
     * @see Joueur#ajouterRess(String, int)
     */
	public void finDeTour(){
		joueur.ajouterRess("Bois", productionParTour);
	}
    /**
     * Modifie la production par tour en l'augementant par un nombre donne en parametre
     * @param i Le nombre avec lequel on augmentera la production par tour
     */
	public void setProduc(int i){
		productionParTour+=i;
	}
    /**
     * Renvoie le type de Batiment
     * @return Un String indiquant qu'il s'agit d'une scierie
     */
	public String toString(){
		return "Scierie";
	}
    /**
     * Permet d'avoir la String correspondant à l'image que l'on cherchera pour représenter la scierie
     * @return Un String correspondant au nom de l'image d'une scierie
     */
	@Override
	public String identImage() {
		return "scierie";
	}
}