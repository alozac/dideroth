package Modele;

public class BatimentFactory {

	public static Batiment creerBatiment(Plateau p, String bat, Joueur j){
		switch(bat.toLowerCase()){
		case "scierie": 
			return new Scierie(j, p);
		case "mine":
			return new Mine(j, p);
		case "tour":
			return new Tour(j, p);
		}
		return null;
	}
}
