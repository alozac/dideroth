package Modele;
/**
 * Lac est une classe heritee de TypeTerrain
 * une Case ayant Lac comme Type de terrain n'est pas franchissable
 * @see TypeTerrain
 */
public class Lac extends TypeTerrain    {
	
	/**
	 * Construit un lac "super" correspond au constructeur de la classe
	 * TypeTerrain
	 * 
	 * @param type l'identifiant du type de terrain
	 * @param image le chemin d'acces à l'image representant le type de terrain
	 * @param nom le nom du type de terrain
	 * @param franchissable indique si le type de terrain est frnchissable par les unités ou non
	 */
	public Lac (String type, String image, String nom, boolean franchissable){
		super(type, image , nom, franchissable);
	}

}
