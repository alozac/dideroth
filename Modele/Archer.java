package Modele;

/**
 * Archer est la classe implémentant l'unité des archers, qui combattent à distance
 * Elle hérite de la classe ADistance
 * @see ADistance
 */
public class Archer extends ADistance{
		
		/**
		 * Constructeur d'Archer
		 * @param j le joueur dirigeant l'unité
		 * @see Unite#remplirCout(int, int, int)
		 */
    public Archer(Joueur j){
    	//pv, pdd, nbCoups, agi, porteeD, porteeA, nivNece, joueur
    	super(10, 3, 2, 0.6, 3, 5, 1, j);
    	remplirCout(60,5,5);
    }
 		
 		/**
 		 * Renvoie la chaîne de caractère correspondant au type de l'unité
 		 * @return String
 		 */
    public String toString(){
		return "Archer";
	}
  
   
}
