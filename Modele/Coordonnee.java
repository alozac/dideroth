package Modele;

/**
 * Coordonnee est la classe représentant les coordonnées des cases du plateau
 */
public class Coordonnee {
	
	/**
	 * x est la coordonnée horizontale
	 */
	public int x;
	
	/**
	 * y est la coordonnée verticale
	 */
	public int y;
	
	/**
	 * Constructeur de Coordonnee
	 * @param x un entier correspondant à la coordonnée horizontale
	 * @param y un entier correspondant à la coordonnée verticale
	 */
	public Coordonnee(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Renvoie une chaîne de caractères représentant une Coordonnées
	 * @return String une chaine de la forme "( x , y )"
	 */
	public String toString (){
		return "( "+ x + " , "+ y +" )";
	}

}
