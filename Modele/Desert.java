package Modele;

/**
 * Desert est la classe implémentant la carte désert
 * Elle hérite de la classe TypeTerrain
 * @see TypeTerrain
 */
public class Desert extends TypeTerrain{

	/**
	 * Constructeur de Desert
	 * @param type une chaîne de caractères identifiant le type de terrain
	 * @param image une chaîne de caractères indiquant le nom de l'image à afficher
	 * @param nom une chaîne de caractères indiquant le nom de la carte
	 * @param franchissable un booléen indiquant s'il est possible ou non de franchir le type de terrain en question
	 */
	public Desert(String type, String image, String nom, boolean franchissable){
		super(type, image, nom, franchissable);
	}

}
