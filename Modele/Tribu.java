package Modele;

/** La classe Tribu représente les différentes tribus du jeu
 */
public abstract class Tribu {
	String nom;
	double bonusVie, bonusAttaque;
	/** 
     * Construit une tribu avec les caracteristiques donnees en argument
     * @param n Le nom de la tribu
     * @param bonVie Son bonus de vie
     * @param bonAtt Son bonus d'attaque
     */
	public Tribu(String n, double bonVie, double bonAtt){
		nom = n;
		bonusVie = bonVie;
		bonusAttaque = bonAtt;
	}
	
	abstract void ajoutePdvSuppl(Unite u);
		
	abstract void ajouteAttaqueSuppl(UniteCombat u);
     /** Renvoie la chaine de caracteres correspondant au nom du tribu courant
     */
	public String toString(){
		return nom;
	}
    /**
     * Ajoute le bonus correspondant au type de tribu courant
     * @param u L'unite qui obtiendra le bonus
     * @see Tribu#ajoutePdvSuppl(Unite)
     * @see Tribu#ajouteAttaqueSuppl(Unite)
     */
	public void bonusDeTribu(Unite u) {
		if (u instanceof UniteCombat){
			ajoutePdvSuppl(u);
			ajouteAttaqueSuppl((UniteCombat) u);
		}
	}
}
