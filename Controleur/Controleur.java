package Controleur;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.io.File;

import javax.swing.ImageIcon;

import InterfaceG.EcranJeu;
import Modele.*;

public class Controleur{
	/**
	 * Le Jeu à mettre en relation avec la Vue
	 */
	public Jeu jeu;
	/**
	 * Le plateau de jeu
	 */
	public Plateau plateau;
	/**
	 * L'écran de jeu à mettre en relation avec le Modèle
	 */
	public EcranJeu ecran;
	/**
	 * Le type de carte choisi par l'utilisateur
	 */
	public String typeCarte;
	/**
	 * Le chemin d'accès aux fichiers
	 */
	public String chemin = "/";
	/**
	 * Tableau de String listant les noms d'image des différents types d'unité et de bâtiments
	 */
	private String[] ficIm = {"Magicien",
					  "Chevalier",
					  "Archer",
					  "Machine",
					  "Dragon",
					  "Monstre",
					  "chateau",
					  "tour",
					  "scierie",
					  "mine",};
	/** 
	 * Les différentes images utilisées pendant le jeu
	 */
	private HashMap<String, ImageIcon> images = new HashMap<String, ImageIcon>();
	private Action actionEnCours;

	/**
	 * Construit un constructeur en chargeant les images utilisées
	 */
	public Controleur(){chemin = new File("").getAbsolutePath()+"/";
		String [] s = {"blue", "red"};
		for (int i = 0 ; i<7 ; i++){
			for (int j=0; j<2; j++){
				images.put(s[j]+ficIm[i], new ImageIcon(chemin + "Images/" + s[j] +ficIm[i] + ".png"));
				if (i==6){
					images.put(s[j]+ficIm[i] + "Entier", new ImageIcon(chemin + "Images/" + s[j]+ficIm[i] + "Entier.png"));
					images.put(s[j]+ficIm[i] + "1", new ImageIcon(chemin + "Images/" +s[j]+ ficIm[i] + "1.png"));
					images.put(s[j]+ficIm[i] + "2", new ImageIcon(chemin + "Images/" + s[j]+ficIm[i] + "2.png"));
					images.put(s[j]+ficIm[i] + "3", new ImageIcon(chemin + "Images/" + s[j]+ficIm[i] + "3.png"));
					images.put(s[j]+ficIm[i] + "4", new ImageIcon(chemin + "Images/" + s[j]+ficIm[i] + "4.png"));
			
				}
			}
		}
		images.put(ficIm[5], new ImageIcon(chemin + "Images/" + ficIm[5] + ".png"));
		for (int i = 7 ; i<ficIm.length ; i++) {
			images.put(ficIm[i] + "Entier", new ImageIcon(chemin + "Images/" + ficIm[i] + "Entier.png"));
			images.put(ficIm[i] + "1", new ImageIcon(chemin + "Images/" + ficIm[i] + "1.png"));
			images.put(ficIm[i] + "2", new ImageIcon(chemin + "Images/" + ficIm[i] + "2.png"));
			images.put(ficIm[i] + "3", new ImageIcon(chemin + "Images/" + ficIm[i] + "3.png"));
			images.put(ficIm[i] + "4", new ImageIcon(chemin + "Images/" + ficIm[i] + "4.png"));
		}
		images.put("coffre", new ImageIcon(chemin + "Images/" + "coffre.png"));
	}
	
	/**
	 * Initialise l'attribut "jeu" et charge les images des types de terrain associés au type de carte choisi
	 * @param paramModeCoop est un tableau de int stockant le nombre de monstres et le nombre de tours choisis. 
	 * @param typeJeu est une String qui détermine le mode de jeu (joueur versus joueur ou joueurs versus ennemi).
	 * @param typeCarteSel est une String représentant le type de carte sélectionné.
	 * @param typeTribuJ2 est une String qui représente la tribu choisie par le joueur 2.
	 * @param typeTribuJ1 est une String qui représente la tribu choisie par le joueur 1.
	 */
	public void creationJeu(String typeTribuJ1, String typeTribuJ2, String typeCarteSel, String typeJeu, int[] paramModeCoop) {
		typeCarte = typeCarteSel;
		if (typeJeu.matches("<.*>Joueur versus Joueur<.*>"))jeu = new JeuVS (chemin + "FichiersCartes/", typeCarte, typeTribuJ1, typeTribuJ2);
		else jeu = new JeuCoop (chemin + "FichiersCartes/", typeCarte, typeTribuJ1, typeTribuJ2, paramModeCoop);
		plateau = jeu.getPlateau();
		for (Entry<String, String> c : plateau.getImagesTypesTerr().entrySet())
			images.put(c.getKey(), new ImageIcon(chemin + "Images/TypesTerrains/" + c.getValue()));
	}
	
	/**
	 * Initialise l'attribut "écranJeu" et ajoute cet écran comme observateur du jeu
	 * @param ecranJeu L'écran de jeu à ajouter
	 */
	public void ajouterEcran(EcranJeu ecranJeu) {
		ecran = ecranJeu;
		jeu.addObserver(ecranJeu);
	}
	
	/**
	 * Effectue les actions associées à la fin de tour d'un joueur
	 */
	public void finTour(){
		String rap = getJoueurCourant().finDeTour();
		actionEnCours = null;
		ecran.carte.effacerCasesAColor();
		ecran.carte.sauvegarderPosition(getJoueurCourant().getNum());
		jeu.joueurSuivant();
		//plateau.nouveauxCoffres();
		if (getJoueurCourant().getNum() == 1) {
			jeu.numTour++;
			if (jeu instanceof JeuCoop) rap +=((JeuCoop)jeu).attaqueEnnemisNeutres();
		}
		if (jeu.partieTerminee()){
			ecran.fenetreVictoire(0);
			jeu = null;
			return;
		}
		ecran.carte.miseAJour();
		ecran.panneauDroit.ajouterCombat(rap);
		miseAJourInterface();
	}

	/**
	 * Met à null l'attribut "jeu"
	 */
	public void quitter() {
		Joueur.nbJoueurs = 0;
		jeu = null;
	}
	
	/**
	 * Détermine l'action à effectuer lors du clic sur la case de coordonnées données en paramètres.
	 * @param coordX La coordonnée x de la case où a cliqué le joueur
	 * @param coordY La coordonnée y de la case où a cliqué le joueur
	 */
	public void actionClic(int coordX, int coordY) {
		if (actionEnCours instanceof Achat){
			construireBat(coordX, coordY);	
			return;
		}
		if (actionEnCours instanceof Mouvement)
			deplacerSiPossible(coordX, coordY);
		Unite unit = plateau.getCase(coordX, coordY).getOccupant();
		if (unit instanceof Tour)
			determinerCasesAColorTour((Tour) unit);
		if (unit != null){
			choixUniteADeplacer(coordX, coordY);
			determinerCasesAColorMvt();
		}
	}

	/**
	 * Effectue l'amélioration des bâtiments du joueur courant
	 */
	public void ameliorer(){
		new Ameliorer(getJoueurCourant()).effectuer_action();
		miseAJourInterface();
	}
	
	/**
	 * Détermine si le joueur courant peut ou non améliorer ses bâtiments
	 * @return	La possibilité d'amélioration
	 */
	public boolean amelChateauPossible(){
		return new Ameliorer(getJoueurCourant()).amelPossible();
	}
	
	/**
	 * Détermine si le joueur courant peut ou non acheter le Bâtiment dont la représentation textuelle est donnée en paramètre
	 * @param bat La représentation textuelle d'un bâtiment
	 * @return La possibilité d'achat du bâtiment
	 */
	public boolean achatBatPossible(String bat) {
		setActionEnCours(bat);
		boolean b = ((Achat)actionEnCours).achatPossible();
		actionEnCours = null;
		return b;
	}
	
	/**
	 * Si possible, effectue la construction du Bâtiment sélectionné par le joueur (stocké dans "actionEnCours")
	 * sur la case de coordonnées (x, y). Ne fait rien sinon.
	 * @param x La coordonnée x de la case où  construire le bâtiment
	 * @param y La coordonnée y de la case où  construire le bâtiment
	 */
	public void construireBat(int x, int y){
		if (((Achat) actionEnCours).constructionPossible(new Coordonnee(x,y))){
			((Achat) actionEnCours).effectuer_action();
			actionEnCours = null;
			miseAJourInterface();
		}
	}

	/**
	 * Effectue l'action Recruter à partir de la représentation textuelle d'une unité
	 * et de la quantité d'unités à recruter données en paramètre
	 * @param unite La représentation textuelle de l'unité à recruter
	 * @param nbU	La quantité d'unité à recruter
	 */
	public void recruter(String unite, int nbU) {
		new Recruter(getJoueurCourant(), unite, nbU).effectuer_action();
		miseAJourInterface();
	}
	
	/**
	 * Détermine la quantité d'unités que le joueur peut recruter
	 * @param unit	Le type de l'unité à recruter	
	 * @return Le nombre maximal d'unités que le joueur peut recruter
	 */
	public int getNbRecrutPossible(String unit) {
		return new Recruter(getJoueurCourant(), unit, 0).determinerMaxPossible(UniteFactory.creerUnite(unit, getJoueurCourant()));
	}
	
	/**
	 * Initialise "actionEnCours" comme étant un nouveau Mouvement si la case de coordonnées (x,y)
	 * est une bonne case de départ pour un déplacement d'unité. Le met à "null" sinon.
	 * @param x La coordonnée x de la case de départ pour le nouveau Mouvement
	 * @param y La coordonnée y de la case de départ pour le nouveau Mouvement
	 * @see #actionClic(int, int)
	 */
	public void choixUniteADeplacer(int x, int y) {
		Case depart = plateau.getCase(x, y);
		actionEnCours = new Mouvement(getJoueurCourant(), plateau, depart);
		if (((Mouvement) actionEnCours).verifierCase("depart"))
			((Mouvement) actionEnCours).setCasesAPortee();
		else actionEnCours = null;
	}
	
	/**
	 * Effectue le Mouvement (ou l'Attaque en fonction de la case d'arrivée dont les coordonnées sont données en paramètre) si cela est possible,
	 * puis vérifie si la partie est terminée ou non.
	 * @param x La coordonnée x de la case d'arrivée pour le Mouvement
	 * @param y La coordonnée y de la case d'arrivée pour le Mouvement
	 * @see #choixUniteADeplacer(int, int)
	 * @see #deplacerSiPossible(int, int)
	 */
	public void deplacerSiPossible(int x, int y) {
		Case arrivee = plateau.getCase(x, y);
		((Mouvement) actionEnCours).setArrivee(arrivee);
		String rapport = null;
		if (arrivee.estLibre()){
			if (((Mouvement) actionEnCours).verifierCase("arrivee")) ((Mouvement)actionEnCours).effectuer_action();
			else  actionEnCours = null;
		} else {
			actionEnCours = new Attaquer((Mouvement)actionEnCours, arrivee.getOccupant());
			if (!((Attaquer) actionEnCours).verifierCase("arrivee")) actionEnCours = null;
			else {
				((Attaquer)actionEnCours).effectuer_action();
				rapport = ((Attaquer)actionEnCours).rapport;
			}
		}
		if (jeu.partieTerminee()){
			if (jeu instanceof JeuCoop)
				ecran.fenetreVictoire(-1);
			else ecran.fenetreVictoire(jeu.getGagnant().getNum());
			return;
		}
	    for (Joueur j : jeu.getJoueurs()) j.getChateau().miseAJourEmplRecrut();
		actionEnCours = null;
		miseAJourInterface();
		ecran.panneauDroit.ajouterCombat(rapport);
	}

	/**
	 * Permet de naviguer entre les unités du joueur courant n'ayant pas encore attaqué.
	 * @param direction	Permet de déterminer si il faut aller à l'unité suivante, ou à l'unité précédente.
	 */
	public void changeUnit(String direction){
		ArrayList<UniteCombat> unit = getJoueurCourant().getUnites();
		int u = getJoueurCourant().getUniteCourante();
		int tmp = u;
		for (int i = 0; i<unit.size(); i++){
			if (direction.equals(">")) tmp = ((u +1+ i)%unit.size());
			else tmp = ((u-1- i)%unit.size()+unit.size())%unit.size();
			getJoueurCourant().setUniteCourante(tmp);
			if (!unit.get(tmp).aAttaque()){
				Coordonnee c=unit.get(tmp).getPosition().coord;
				changerVisionCarte(new Point(c.y, c.x ), true);
				return;
			}
		}
	}

	/**
	 * Lorsque l'action en cours est un Mouvement, remplis les listes "casesAColor" de la classe Carte
	 * à partir des cases à portée déterminées par le Mouvement en cours
	 */
	public void determinerCasesAColorMvt() {
		if (actionEnCours instanceof Mouvement){
			ecran.carte.casesAColorV.addAll(((Mouvement) actionEnCours).getCasesAPorteeMvt());
			ecran.carte.casesAColorR.addAll(((Mouvement) actionEnCours).getCasesAPorteeAtt());
			ecran.carte.casesAColorB.addAll(((Mouvement) actionEnCours).getCasesAPorteeSoins());
		} 
	}
	
	/**
	 * Remplis la liste "caseAColorR" de Carte avec les cases a portée d'attaque de la Tour donnée en paramètre.
	 * @param tour La Tour sur laquelle le joueur a cliqué pour connaître ses cases à portée d'attaque.
	 */
	public void determinerCasesAColorTour(Tour tour) {
		for (Case c : tour.getCasesAPortee())
			ecran.carte.casesAColorR.add(c.coord);
	}
	
	/**
	 * Lorsque l'action en cours est un achat de Batiment, remplis soit "casesAColorR" soit "casesAColorV" de Carte
	 * en fonction de la case de coordonnéees (x,y) où se trouve le pointeur de la souris
	 * @param x La coordonnée x de la case où se trouve le pointeur de la souris
	 * @param y La coordonnée y de la case où se trouve le pointeur de la souris
	 */
	public void determinerCasesAColorAchatBat( int x, int y) {
		ecran.carte.casesAColorB.clear();
		if (determCouleurAchatBat(x, y) == Color.RED){
			ecran.carte.casesAColorR = ajouterCasesAColorAchatBat(x,y);
			ecran.carte.casesAColorV.clear();
		}else {
			ecran.carte.casesAColorV = ajouterCasesAColorAchatBat(x,y);
			ecran.carte.casesAColorR.clear();
		}
	}

	/**
	 * Lors d'un achat de Bâtiment, détermine la couleur dans laquelle colorier les cases qu'occuperait le bâtiment
	 * si on le construisait à partir de la case de coordonnées (x,y) (un bâtiment peut occuper une ou plusieurs cases).
	 * @param x La coordonnée x de la case où se trouve le pointeur de la souris
	 * @param y La coordonnée y de la case où se trouve le pointeur de la souris
	 * @return La couleur dans laquelle colorier les cases : vert si la construction du bâtiment est possible, et rouge sinon.
	 * @see #determinerCasesAColorAchatBat(int, int)
	 */
	private Color determCouleurAchatBat(int x, int y) {
		if (actionEnCours != null){
			if (((Achat)actionEnCours).constructionPossible(new Coordonnee(x, y)))
					return Color.GREEN;
			return Color.RED;
		}
		return null;
	}
	
	/**
	 * Lors de l'achat d'un bâtiment, renvoie la liste des cases à colorier, c'est-à-dire les cases qu'occuperait le bâtiment
	 * si on le construisait à partir de la case de coordonnées (x,y) (un bâtiment peut occuper une ou plusieurs cases).
	 * @param x La coordonnée x de la case où se trouve le pointeur de la souris
	 * @param y La coordonnée y de la case où se trouve le pointeur de la souris
	 * @return La liste des cases à colorier.
	 * @see #determinerCasesAColorAchatBat(int, int)
	 */
	private ArrayList<Coordonnee> ajouterCasesAColorAchatBat(int x, int y) {
		ArrayList<Coordonnee> listCases = new ArrayList<Coordonnee>();
		for (int i = 0 ; i<((Achat)actionEnCours).bat.getTailleB(); i++)
		for (int j = 0 ; j<((Achat)actionEnCours).bat.getTailleB(); j++)
			listCases.add(new Coordonnee(x+i,y+j));
		return listCases;	
	}
	
	/**
	 * Modifie le couple (x,y) de Carte symbolisant le coin supérieur gauche de la partie du plateau
	 * à afficher à partir du Point donné en paramètre.
	 * @param point Le Point contenant les nouvelles valeurs du couple (x,y)
	 * @param changmtUnit Détermine s'il faut faire des opérations sur le nouveau coupe (x,y), en fonction de la méthode qui appelle "changerVisionCarte"
	 */
	public void changerVisionCarte(Point point, boolean changmtUnit) {
		if (changmtUnit) {
			ecran.carte.x=point.y - ecran.carte.getZoomL()/2;
			ecran.carte.y=point.x - ecran.carte.getZoomL()/2;
			ecran.carte.setCaseSurbr(point);
		} else{
			ecran.carte.x=point.y;
			ecran.carte.y=point.x;
		}
		ecran.carte.deplacerCarte(' ');
	}
	
	/**
	 * Détermine l'image de l'occupant situé sur la case de coordonnées (i,j)
	 * @param i	La coordonnée x de la case dont on souhaite connaître l'occupant
	 * @param j La coordonnée y de la case dont on souhaite connaître l'occupant
	 * @param imEntiere	Si l'occupant occupe plusieurs cases, ce paramètre sert à renvoyer soit l'image entière de l'occupant (dans le PanneauDroit),
	 * soit une partie de l'image (sur la Carte)
	 * @return L'image associée à l'occupant de la case de coordonnées (i,j).
	 */
	public ImageIcon determinerImageOcc(int i, int j, boolean imEntiere) {
		Case c = plateau.getCase(i, j);
		if (c == null) return null;
		Unite u = c.getOccupant();
		if (u instanceof Monstre) return images.get("Monstre");
		if (u == null) 
			return c.existeRessources() ? images.get("coffre") : null;
		String coulJ = u.getJoueur().getCoulJoueur().equals("Bleu")?"blue" : "red";
		if (u instanceof UniteCombat)  return images.get(coulJ + u.identImage());
		if (u instanceof Batiment) {
			if (u instanceof Chateau){
				return images.get(coulJ+(imEntiere? "chateauEntier":"chateau"+(((Batiment) u).getPositions().indexOf(c)+1)));
			}
			if (imEntiere) return images.get(((Batiment)u).identImage() + "Entier");
			return images.get(((Batiment)u).identImage() + (((Batiment) u).getPositions().indexOf(c)+1));
		}
		return null;
	}
	
	/**
	 * Détermine l'image du type de terrain situé sur la case de coordonnée (i,j)
	 * @param i La coordonnée x de la case dont on souhaite connaître le type de terrain
	 * @param j La coordonnée y de la case dont on souhaite connaître le type de terrain
	 * @return Limage associée au type de terrain de la case de coordonnées (i,j).
	 */
	public ImageIcon determinerImageTerrain(int i, int j){
		Case c = plateau.getCase(i, j);
		if (c == null) return null;
		return images.get(c.getTerrain().toString());
	}
	
	/**
	 * Met à jour l'interface graphique.
	 */
	public void miseAJourInterface(){
		jeu.setChanged();
		jeu.notifyObservers();	
	}
	
	/**
	 * Renvoie le nombre de ressources de type donné en paramètre du joueu courant
	 * @param typeRess Le type de ressources voulu ("Or", "Minerai" ou "Bois")
	 * @return Le nombre de ressources de type "typeRess"
	 */
	public String getRessources (String typeRess){
		return ""+getJoueurCourant().getRess(typeRess);
	}
	
	
	/**
	 * Détermine si l'occupant de la case de coordonnées (i,j) est un allié du joueur courant ou non.
	 * @param i	La coordonnée x de la case dont on souhaite connaître le camp de l'occupant.
	 * @param j La coordonnée y de la case dont on souhaite connaître le camp de l'occupant.
	 * @return true si l'occupant est un allié, false sinon.
	 */
	public boolean estAllie(int i, int j) {
		return getJoueurCourant().getUnites().contains(plateau.getCase(i,j).getOccupant());
	}
	
	/**
	 * Initialise "actionEnCours" en un nouvel Achat de Batiment
	 * @param bat La représentation textuelle du Batiment utilisé dans la construction du nouvel Achat
	 */
	public void setActionEnCours(String bat) {
		actionEnCours = new Achat(plateau, getJoueurCourant(), bat);
	}
	
	/**
	 * Met à null l'action en cours et efface les listes "casesAColor" de Carte (méthode appelée lors de l'appui sur la touche "Echap").
	 */
	public void annulerAction() {
		actionEnCours = null;
		ecran.carte.effacerCasesAColor();
	}
	
	/**
	 * Détermine si l'action en cours est un achat de bâtiment ou non.
	 * @return true si ActionEnCours est un Achat, false sinon.
	 */
	public boolean achatEnCours() {
		return actionEnCours instanceof Achat;
	}
	
	/**
	 * Détermine si l'action en cours est un mouvement ou non.
	 * @return true si ActionEnCours est un Mouvement, false sinon.
	 */
	public boolean mvtCommence() {
		return actionEnCours instanceof Mouvement;
	}
	
	/**
	 * Renvoit le joueur dont c'est actuellement le tour.
	 * @return Le joueur courant de "jeu".
	 */
	public Joueur getJoueurCourant(){
		return jeu.getCourant();
	}

	/**
	 * Renvoie le Chateau du joueur dont c'est le tour.
	 * @return Le Chateau du joueur courant.
	 */
	public Batiment getChateau() {
		return getJoueurCourant().getChateau();
	}

	/**
	 * Renvoie le nombre de points d'action restant au joueur dont c'est le tour.
	 * @return Le nombre de points d'action du joueur courant.
	 */
	public int getNbPARestants() {
		return getJoueurCourant().getPa();
	}
	
	/**
	 * Renvoie la couleur du joueur dont c'est le tour.
	 * @return La couleur du joueur courant.
	 */
	public String getCouleur(){
		if (getJoueurCourant().getNum()==1) return "Bleu";
		return "Rouge";
	}
	
	/**
	 * Renvoie la représentation textuelle de l'occupant de la case de coordonnées (x,y).
	 * @param x La coordonnée x de la case dont on souhaite connaître l'occupant
	 * @param y La coordonnée y de la case dont on souhaite connaître l'occupant
	 * @return La représentation textuelle de l'occupant si il y en a un, et " - " sinon.
	 */
	public String getTypeUnit(int x, int y) {
		Unite u = plateau.getCase(x, y).getOccupant();
		if (u != null) return u.toString();
		return " - ";
	}
	
	/**
	 * Renvoie un tableau contenant les différentes caractéristiques de l'occupant de la case de coordonnées (x,y),
	 * qui seront affichées dans le PanneauDroit
	 * @param x La coordonnée x de la case dont on souhaite connaître l'occupant
	 * @param y La coordonnée x de la case dont on souhaite connaître l'occupant
	 * @return Les différentes caractéristiques de l'occupant (ses points de vie dans le premier élément du tableau, son attaque dans le deuxième, etc).
	 * Si il ne possède pas certaines caractéristiques (par exemple un Chateau n'a pas de points d'attaque),
	 * les éléments du tableau associés à ces caractéristiques sont remplis par " - ".
	 */
	public String[] getCaractUnite(int x, int y) {
		String [] caract = new String[6];
		Unite u = plateau.getCase(x, y).getOccupant();
		if (u != null){
			caract[0] = "" + u.getRatioVie();
			if (u instanceof UniteCombat){
				caract[1] = ((UniteCombat)u).getRatioAttaque();
				caract[3] = "" +((UniteCombat)u).getAgilite();
				caract[2] = ((UniteCombat)u).getPortees();
			}
			else if (u instanceof Tour){
				caract[1] = "" + ((Tour)u).getPdd();
				caract[2] = "0 - " + ((Tour)u).getPorteeA();
			}
			else if (u instanceof Batiment) caract[4] = "" + ((Batiment)u).getProduction();
		}
		for (int i = 0 ; i < caract.length ; i++)
			if (caract[i] == null) caract[i] = " - ";
		return caract;
	}

	/**
	 * Renvoie le couple (x,y) actuel de la carte.
	 * @return Un Point contenant les valeurs du couple (x,y) de la carte.
	 */
	public Point getPosCarteActuelle() {
		return new Point(ecran.carte.x, ecran.carte.y);
	}
	
	/**
	 * Renvoie les zooms zoomL et zoomH actuels de la Carte.
	 * @return Un Point contenant les valeurs des zooms zoomL et zoomH de la carte.
	 */
	public Point getZoomCarte(){
		return new Point(ecran.carte.getZoomL(), ecran.carte.getZoomH());
	}

	/**
	 * Modifie la valeur du zoom de la Carte.
	 * @param value la nouvelle valeur de zoomL
	 */
	public void changeZoom(int value) {
		ecran.carte.setZoom(value);
		ecran.carte.repaint();
	}
	
	/**
	 * Lors de la construction de l'écran de jeu, détermine quelle largeur (en pixel) devra prendre le PanneauDroit en fonction de la taille du plateau.
	 * Cette largeur varie car la taille du plateau fait varier la taille de la minimap, qui est contenue dans PanneauDroit.
	 * @return La largeur que prendra le PanneauDroit
	 */
	public int determLargPanB() {
		int taille = 0;
		int taillePx = 0;
		int tailleP = plateau.taille;
		while (taille < 200){
			taille += tailleP;
			taillePx++;
		}
		return taillePx*tailleP;
	}
	
	/**
	 * Détermine la couleur de fond de la Minimap en fonction du type de carte choisi.
	 * @return La couleur de la Minimap.
	 */
	public Color determinerCoulMinimap() {
		switch(typeCarte.toLowerCase()){
		case "desert":
			return new Color(234,170,82);
		case "montagne":
			return new Color(204,255,255);
		case "foret":
			return new Color(51,204,102);
		}
		return null;
	}

	/**
	 * Renvoie un entier correspondant à la couleur à afficher dans le pixel associé à la case de coordonnée (i,j) dans la Minimap.
	 * @param i La coordonnée x de la case dont on souhaite connaître la couleur
	 * @param j La coordonnée x de la case dont on souhaite connaître la couleur
	 * @return 0 si il n'y a pas d'occupant sur la case, 3 s'il s'agit d'un Monstre, et le numéro du joueur à qui appartient l'occupant sinon.
	 */
	public int getCouleurPixel(int i, int j) {
		Unite u = plateau.getCase(new Coordonnee(i,j)).getOccupant();
		if ( u instanceof Monstre) return 3;
		if (u != null) return u.getJoueur().getNum();
		return 0;
	}

	/**
	 * Détermine si le jeu est en mode coopération ou non.
	 * @return true si le jeu est un JeuCoop, false sinon.
	 */
	public boolean jeuCoop() {
		return jeu instanceof JeuCoop;
	}

	/**
	 * Renvoie une chaine représentant le numéro de tour actuel, et le nombre total de tours avant la fin de la partie si le jeu est en mode coopération
	 * @return le numéro de tour actuel, et le nombre total de tours si besoin
	 */
	public String getNumTour() {
		if (jeu instanceof JeuCoop){
			int nbTMax = ((JeuCoop) jeu).getNbToursMax();
			return jeu.numTour + " / " + (nbTMax==-1?"inf":nbTMax);
		}
		return "" + jeu.numTour;
	}

	/**
	 * Renvoie le nombre de monstres vaincus par les joueurs et le nombres total de monstres placés sur le terrain en début de partie;=.
	 * @return Une chaine représentant le nombre de monstres.
	 */
	public String getNbMonstres() {
		if (jeu instanceof JeuCoop)
			return ((JeuCoop) jeu).getNbMonstres() + " / " + ((JeuCoop)jeu).getNbTotalMonstres();
		return "" + jeu.numTour;
	}

	/**
	 * Renvoie le nom de la tribu du joueur courant.
	 * @return la tribu du joueur courant.
	 */
	public String getTribu() {
		return getJoueurCourant().getTribu().toString();
	}
	
	/**
	 * Permet d'obtenir le cout d'une unité en or, bois, et minerai et le niveau minimum nécéssaire du château
	 * pour qu'ils apparaissent dans la fênetre d'achat.
	 * @param unit Une chaine qui correspond au type de l'unité.
	 * @return une HashMap avec comme clef le type de ressource,
	 * et comme valeur le cout correspondant ou le niveau nécéssaire.
	 */
	public HashMap<String,Integer> getCoutToolTipU (String unit){
		Unite u = UniteFactory.creerUnite(unit, jeu.getCourant());
		HashMap<String, Integer> hm = u.getCout();
		hm.put("NiveauNece", u.getNivChateauNece());
		return hm;
	}
	
	/**
	 * Permet d'obtenir le cout d'un bâtiment en or, bois, et minerai et le niveau minimum nécéssaire du château
	 * pour qu'ils apparaissent dans la fênetre d'achat.
	 * @param bat Une chaine qui correspond au type du bâtiment.
	 * @return une HashMap avec comme clef le type de ressource,
	 * et comme valeur le cout correspondant  ou le niveau nécéssaire.
	 */
	public HashMap<String, Integer> getCoutToolTipB (String bat){
		Batiment b = BatimentFactory.creerBatiment(plateau, bat, jeu.getCourant());
		HashMap<String, Integer> hm = b.getCout();
		hm.put("NiveauNece", b.getNivChateauNece());
		return hm;
	}

}
