package InterfaceG;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;

/**
 * Represente la fen�tre où sont affichees les regles du jeu
 */
public class Regles extends JFrame {
	/**
	 * Le constructeur de la classe Regles, ou l'on choisit la taille de la
	 * fenetre, le titre de la fenetre, etc
	 */
	public Regles() {
		this.setTitle("Dideroth - règles du jeu");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setMinimumSize(new Dimension(700, 700));
		this.pack();
		this.setVisible(true);
		try {
			JEditorPane editorPane = new JEditorPane("file:///"+new File("").getAbsolutePath() + "/Regle/regles.html"); 
			editorPane.setEditable(false);

			HyperlinkListener hyperlinkListener = new ActivatedHyperlinkListener(editorPane);
			editorPane.addHyperlinkListener(hyperlinkListener);

			JScrollPane scrollPane = new JScrollPane(editorPane);
			this.add(scrollPane);
		} catch (IOException e) {
			System.err.println("Impossible de charger: " + e);
		}
	}

	class ActivatedHyperlinkListener implements HyperlinkListener {

		JEditorPane editorPane;

		public ActivatedHyperlinkListener(JEditorPane editorPane) {
			this.editorPane = editorPane;
		}

		public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
			HyperlinkEvent.EventType type = hyperlinkEvent.getEventType();
			final URL url = hyperlinkEvent.getURL();
			if (type == HyperlinkEvent.EventType.ENTERED) {
				System.out.println("URL: " + url);
			} else if (type == HyperlinkEvent.EventType.ACTIVATED) {
				System.out.println("Activated");
				Document doc = editorPane.getDocument();
				try {
					editorPane.setPage(url);
				} catch (IOException ioException) {
					System.out.println("Error following link");
					editorPane.setDocument(doc);
				}
			}
		}
	}
}
