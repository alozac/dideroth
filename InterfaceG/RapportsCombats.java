package InterfaceG;

import java.awt.BorderLayout;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Controleur.Controleur;
/**
 * Permet de créer un JPanel où l'on trouve les rapports de combat.
 */
class RapportsCombats extends JPanel{
	String listRap = "";
	JEditorPane combats;
	Controleur controleur;
	/**
	 * Permet de créer le RapportsCombats
	 * @param control est un Controleur qui correspond au controleur relié au modèle.
	 */
	public RapportsCombats(Controleur control){
		controleur = control;

		setLayout(new BorderLayout());
		
		combats = new JEditorPane("text/html", listRap);
		combats.setEditable(false);
		combats.setOpaque(false);
		
		JScrollPane jsp = new JScrollPane(combats);
		add(jsp, BorderLayout.CENTER);
	}
	/**
	 * Permet d'ajouter un combat dans le rapport de combat.
	 * @param rapport est une String qui correspond au combat qui vient de se dérouler (qui a attaqué, nombre de points de vie perdus, etc).
	 */
	public void ajouterCombat(String rapport){
		if (rapport == null) return;
		listRap = rapport + listRap;
		combats.setText(listRap);
		combats.setCaretPosition(0);
	}
}