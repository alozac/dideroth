package InterfaceG;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Controleur.Controleur;

public class EcranJeu extends JFrame implements Observer{
	Controleur controleur;
	public Carte carte;
	public PanneauDroit panneauDroit;
	public PanneauBas panneauBas;
	/**
	 * Permet de créer l'écran de jeu, où se trouve la carte.
	 * @param control est un Controleur qui correspond au controleur relié au modèle.
	 */
	public EcranJeu(Controleur control) {
		this.setTitle("Dideroth");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// this.setLocationRelativeTo();
		this.setMinimumSize(new Dimension(1000, 600));
		controleur = control;
		
		controleur.ajouterEcran(this);

		panneauBas = new PanneauBas(this, controleur);
		carte = new Carte(this, controleur);
		panneauDroit = new PanneauDroit(this, controleur);

		JPanel container = new JPanel(new BorderLayout());
		JPanel containerG = new JPanel(new BorderLayout());
		containerG.add(panneauBas, BorderLayout.SOUTH);
		containerG.add(carte, BorderLayout.CENTER);
		container.add(containerG, BorderLayout.CENTER);
		container.add(panneauDroit, BorderLayout.EAST);

		this.setContentPane(container);

		this.setFocusable(true);
		this.requestFocus();
		addKeyListener(new MouvementCarte());

		Toolkit tk = Toolkit.getDefaultToolkit();
		Image img = tk.getImage(controleur.chemin + "Images/curseur.png");
		Cursor monCurseur = tk.createCustomCursor(img, new Point(0, 0), "");
		setCursor(monCurseur);

		this.setVisible(true);
	}
	/**
	 * Permet d'afficher une fenêtre dans le cas où l'un des deux joueurs gagnent, ou si les deux joueurs ont vaincu l'ennemi commun.
	 * @param n est un int qui correspond au numéro du joueur qui a gagné dans le mode de jeu "Joueur versus Joueur". Dans le mode
	 * "Joueurs versus Ennemi", s'il est égal à 0 cela veut dire que les joueurs n'ont pas réussi à tuer l'ennemi commun, s'il est
	 * égal à -1 cela veut dire qu'ils ont réussi.
	 */
	public void fenetreVictoire(int n) {
		JOptionPane conf = new JOptionPane();
		String message = "";
		if ( n == 0 ) message = "Vous n'avez pas tué assez d'ennemis ! Voulez-vous réessayer?";
		else if (n == -1) message = "Vous avez vaincu tous les ennemis ! Voulez-vous recommencer?";
		else message = "Joueur " + n + " a gagné ! Voulez-vous recommencer?"; 
		int option = conf.showConfirmDialog(null, message, "Fin de jeu",
											JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		this.dispose();
		controleur.quitter();
		if (option == JOptionPane.NO_OPTION) {
			EcranAccueil ecran = new EcranAccueil(controleur);
			ecran.setVisible(true);
		} else {
			EcranOptions ecran = new EcranOptions(controleur);
			ecran.setVisible(true);
		}
	}

	@Override
	public void update(Observable arg0, Object obj) {
		panneauBas.miseAJour();
		panneauDroit.miseAJour();
		repaint();	
	}
	
	class MouvementCarte implements KeyListener {

		@Override
		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode()) {
			case KeyEvent.VK_RIGHT:
				carte.deplacerCarte('r');
				break;
			case KeyEvent.VK_LEFT:
				carte.deplacerCarte('l');
				break;
			case KeyEvent.VK_DOWN:
				carte.deplacerCarte('d');
				break;
			case KeyEvent.VK_UP:
				carte.deplacerCarte('u');
				break;
			case KeyEvent.VK_ESCAPE:
				controleur.annulerAction();
				repaint();
				break;
			}

		}

		public void keyReleased(KeyEvent arg0) {
		}

		public void keyTyped(KeyEvent arg0) {
		}
	}
}
