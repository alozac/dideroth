package InterfaceG;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import Controleur.Controleur;
import Modele.Coordonnee;
import Modele.Plateau;
/**
 * Carte est la classe utilisée pour afficher le plateau de jeu, les types de terrain, les unités,...
 *
 */
public class Carte extends JPanel{
	
	Plateau plateau;
	/**
	 * Le couple (x,y) représente la première case affichée par la carte, située donc dans le coin supérieur droit de la carte
	 */
	public int x, y;
	/**
	 * Ce tableau permet de sauvegarder l'emplacement de la carte (c'est-à-dire la valeur du couple (x,y))
	 * pour chacun des joueurs à la fin de leutr tour 
	 */
	private Point[] svgdePlacmtCarte;
	/**
	 * Ce tableau permet de sauvegarder le nombre de cases visibles en largeur et en hauteur
	 * pour chacun des joueurs à la fin de leur tour
	 * (Ce zoom est initialisé à 20*20 cases en début de partie).
	 */
	private Point[] svgdeZoom = {new Point(20,20), new Point(20,20)};
	/**
	 * La taille du plateau
	 */
	int taille;
	/**
	 * la taille (en pixel) actuelle d'une case;
	 * Ce paramètre varie en fonction du zoom actuel de la carte.
	 */
	int tailleC;
	/**
	 * Le nombre de cases visibles en largeur (initialisé à 20 en début de partie).
	 */
	private int zoomL = 20;
	/**
	 * Le nombre de cases visibles en hauteur (initialisé à 20 en début de partie).
	 */
	private int zoomH = 20;
	/**
	 * La liste des cases à colorer en rouge lors d'un appel à la méthode repaint()
	 * @see #paintComponents(Graphics)
	 */
	public ArrayList<Coordonnee> casesAColorR = new ArrayList<Coordonnee>();
	/**
	 * La liste des cases à colorer en vert lors d'un appel à la méthode repaint()
	 * @see #paintComponent(Graphics)
	 */
	public ArrayList<Coordonnee> casesAColorV = new ArrayList<Coordonnee>();
	/**
	 * La liste des cases à colorer en bleu lors d'un appel à la méthode repaint()
	 * @see Carte#paintComponent(Graphics)
	 */
	public ArrayList<Coordonnee> casesAColorB = new ArrayList<Coordonnee>();
	/**
	 * Le Point représentant la case à faire apparaître en surbrillance
	 * (Lors de la navigation entre les unités n'ayant pas encore attaqué)
	 * @see Controleur#changeUnit(String)
	 */
	public Point caseSurbr;
	/**
	 * Le Controleur assurant la liaison avec le Modèle
	 */
	Controleur controleur;
	
	/**
	 * Constructeur de Carte
	 * @param ecr L'écran de jeu contenant la carte
	 * @param control Le controleur permettant la liaison avec le modèle
	 */
	public Carte(EcranJeu ecr, Controleur control){
		
		setLayout(null);
		
		controleur = control;
		plateau = controleur.plateau;
		taille = plateau.taille;
		
		svgdePlacmtCarte = new Point[2];
		svgdePlacmtCarte[0] = new Point(0,controleur.getChateau().getPosition().coord.y - zoomL/2);
		svgdePlacmtCarte[1] = new Point(taille - zoomH , controleur.getChateau().getPosition().coord.y - zoomL/2);
		
		SurvolCase survol = new SurvolCase();
		this.addMouseListener(survol);
		this.addMouseMotionListener(survol);
		miseAJour();
	}
	
	public void paintComponent(Graphics g){
		setZoom(zoomL);
		if (controleur.ecran.panneauDroit != null) controleur.ecran.panneauDroit.miseAJour();
		g.fillRect(0, 0, getWidth(), getHeight());
		int tailleC = getWidth()/zoomL;
		ImageIcon im = null, imt = null;
		int resteL = getWidth()%zoomL;
		int resteH = getHeight()-zoomH*tailleC;
		int xTmp = x , yTmp = y;
		if (resteH/tailleC>0 && x== taille-zoomH)xTmp = x - resteH/tailleC;
		if (resteL/tailleC>0 && y== taille-zoomL)yTmp = y - resteL/tailleC;
		for (int i = 0 ; i < zoomL + resteL/tailleC + 1;i++){
		for (int j = 0 ; j < zoomH + resteH/tailleC + 1;j++){
			im = controleur.determinerImageOcc(j + xTmp , i + yTmp, false);
			imt=controleur.determinerImageTerrain(j + xTmp , i + yTmp);
			if (imt!=null)g.drawImage(imt.getImage(), i*tailleC, j*tailleC, tailleC, tailleC, this);
			if (im!=null)g.drawImage(im.getImage(), i*tailleC, j*tailleC,tailleC, tailleC, this);
		}
		}
		colorerCases(g);
	}
	
	/**
	 * Modifie les valeurs du couple (x,y) afin de déplacer la carte lors de l'appel à repaint()
	 * @param c La direction dans laquelle déplacer la carte : 
	 * vers le haut pour 'u', bas pour 'd', droite pour 'r' et gauche pour 'l'.
	 */
	public void deplacerCarte(char c){ 
		switch(c){
		case ('r'): if(y<taille-zoomL)y++;break;
		case ('l'): if(y>0)y--;break;
		case ('d'): if(x<taille-zoomH - (getHeight()/tailleC-zoomH))x++;break;
		case ('u'): if(x>0)x--;break;	
		}
		if (x < 0 ) x=0; if (y<0)y=0;
		repaint();
	}
	
	/**
	 * Appelée lors d'un changement de tour, récupère les valeurs du couple (x,y) et du zoom stockées précédemment à la fin du tour précédent du joueur
	 */
	public void miseAJour() {
		int num = controleur.getJoueurCourant().getNum();
		x = svgdePlacmtCarte[num -1].x;
		y = svgdePlacmtCarte[num -1].y;
		zoomL = svgdeZoom[num -1].x;
		zoomH = svgdeZoom[num -1].y;
	}
	
	/**
	 * Modifie les valeurs de zoomL et zoomH. ZoomL est déterminée par le paramètre "value" et zoomH est recalculé à partir de la nouvelle valeur de zoomL
	 * @param value la nouvelle valeur que devra prendre zoomL
	 */
	public void setZoom(int value){
		if (value != -1) zoomL = value;
		zoomH = (zoomL*getHeight())/getWidth();
		if (svgdeZoom[1].y > zoomH)svgdePlacmtCarte[1].x = taille - zoomH;
		if (zoomH>taille) zoomH = taille;
		if (zoomH + x >= taille ) x = taille - zoomH;
		if (zoomL + y >= taille ) y = taille - zoomL;
		tailleC = getWidth()/zoomL;
	}
	
	/**
	 * Colore les cases contenues dans les listes "CasesAColor" dans la bonne couleur (avec transparence),
	 * et ajoute une bordure autour de la case à mettre en surbrillance
	 * @param g L'objet Graphics utilisé pour colorer les cases
	 */
	private void colorerCases(Graphics g) {
		g.setColor(new Color(255,0,0,100));
		for (Coordonnee c : casesAColorR)
			g.fillRect((c.y - y)*tailleC, (c.x - x)*tailleC, tailleC, tailleC);
		g.setColor(new Color(0,255,0,100));
		for (Coordonnee c : casesAColorV)
			g.fillRect((c.y - y)*tailleC, (c.x - x)*tailleC, tailleC, tailleC);
		g.setColor(new Color(0,0,255,100));
		for (Coordonnee c : casesAColorB)
			g.fillRect((c.y - y)*tailleC, (c.x - x)*tailleC, tailleC, tailleC);
		if (caseSurbr != null) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setStroke(new BasicStroke(3f));	
			g2.setColor(Color.ORANGE);
			Shape shape1 = new RoundRectangle2D.Double((caseSurbr.x - y)*tailleC, (caseSurbr.y - x)*tailleC, tailleC, tailleC, 3,3);
			g2.draw(shape1);
		}
	}
	
	/**
	 * Stocke les valeurs du couple (x,y) et des zooms à la fin du tour du joueur
	 * @param numJ Le numéro du joueur dont il faut sauvegarder les données
	 */
	public void sauvegarderPosition(int numJ) {
		svgdePlacmtCarte[numJ - 1] = new Point(x , y);
		svgdeZoom[numJ - 1] = new Point(zoomL, zoomH);
	}
	
	/**
	 * Efface les données contenues dans les listes "casesAColor" et met à "null" caseSurbr
	 */
	public void effacerCasesAColor() {
		casesAColorV.clear();
		casesAColorR.clear();
		casesAColorB.clear();
		caseSurbr = null;
	}
	
	/**
	 * Retourne le nombre de cases visibles en hauteur
	 * @return zoomH
	 */
	public int getZoomH() {
		return zoomH;
	}
	
	/**
	 * Retourne le nombre de cases visibles en largeur
	 * @return zoomH
	 */
	public int getZoomL() {
		return zoomL;
	}

	/**
	 * Modifie la case à mettre en surbrillance
	 * @param p les coordonnées de la case à mettre en surbrillance
	 */
	public void setCaseSurbr(Point p){
		caseSurbr = p;
	}
	
	/**
	 * Classe permettant d'écouter les actions liées aux événements de la souris sur la carte
	 */
	class SurvolCase implements MouseListener, MouseMotionListener{

		/**
		 * Lors du clic de la souris, envoit les coordonnées de la case où a eu lieu le clic au controleur
		 * pour déterminer l'action à effecuer
		 * @Override
		 * @see Controleur#actionClic(int, int)
		 */
		public void mouseClicked(MouseEvent arg0) {
			effacerCasesAColor();
			Point c = arg0.getPoint();
			int coordX = c.y/tailleC + x;
			int coordY = c.x/tailleC + y;
			controleur.actionClic(coordX, coordY);
			repaint();
		}

		/**
		 * Lors du survol de la souris sur la carte, affiche les informations de la case correspondant à la position de la souris
		 * dans PanneauInteractif, contenu dans PanneauDroit
		 * @Override
		 * @see PanneauDroit#infosTour
		 */
		public void mouseEntered(MouseEvent arg0) {
			Point c = arg0.getPoint();
			tailleC = getWidth()/zoomL;
			if (c.x < zoomL*tailleC && c.y < zoomH*tailleC) 
				controleur.ecran.panneauDroit.miseAJourPanInter(c.y/tailleC + x, c.x/tailleC + y);
			if (controleur.achatEnCours())
				controleur.determinerCasesAColorAchatBat(c.y/tailleC + x, c.x/tailleC + y);
			repaint();
		}
		
		/**
		 * Effectue un appel à mouseEntered
		 * @Override
		 * @see mouseEntered(MouseEvent)
		 */
		public void mouseMoved(MouseEvent e) {
			mouseEntered(e);
		}
		
		@Override
		public void mouseExited(MouseEvent arg0) {
		}
		@Override
		public void mousePressed(MouseEvent arg0) {
		}
		@Override
		public void mouseReleased(MouseEvent arg0) {
		}
		@Override
		public void mouseDragged(MouseEvent e) {
		}
		
	}

	
	
}

