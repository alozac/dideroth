package InterfaceG;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import Controleur.Controleur;
import Modele.Plateau;

/**
 * Classe représentant la Minimap du jeu, qui permet aux joueurs de savoir où se trouvent ses unités et celles du second joueur,
 * ainsi que l'emplacement des monstres s'il s'agit d'une partie en mode coopération
 */
public class Minimap extends JPanel{
	
	Color coulTerr;
	Color[] list = {coulTerr, Color.BLUE, Color.RED, Color.BLACK};
	Point posAct;
	Point zoom;
	Controleur controleur;
	Plateau p;
	int tailleP;
	/**
	 * Permet de créer la minimap.
	 * @param control est un Controleur qui correspond au controleur relié au modèle.
	 */
	public Minimap(Controleur control){
		controleur = control;
		p = controleur.plateau;
		tailleP = p.taille;
		coulTerr = controleur.determinerCoulMinimap();
		list[0] = coulTerr;
		zoom = controleur.getZoomCarte();
		this.addMouseListener(new BougerCarte());
		int largeur = controleur.determLargPanB();
		this.setPreferredSize(new Dimension(largeur, largeur));
		
		miseAJour();
		
	}
	/**
	 * Permet de mettre à jour la minimap.
	 */
	void miseAJour(){
		controleur.ecran.setFocusable(true);
		controleur.ecran.requestFocus();
		posAct = controleur.getPosCarteActuelle();
		zoom = controleur.getZoomCarte();
	}
	
	protected void paintComponent(Graphics g){
		miseAJour();
		int taille =  (int) (Math.ceil(200.0/tailleP));
		for (int i = 0; i< tailleP ; i++){
			for (int j = 0; j< tailleP ; j++){
				g.setColor(list[controleur.getCouleurPixel(i, j)]);
				g.fillRect(j*taille,i*taille, taille,taille);
			}
		}
		g.setColor(Color.YELLOW);
		g.drawRect(posAct.y*taille, posAct.x*taille, zoom.x * taille, zoom.y * taille);
	}

	/**
	 * Classe permettant d'écouter les actions liées à la souris sur la minimap.
	 * Les joueurs peuvent ainsi déplacer la carte en cliquant sur la Minimap.
	 * 
	 */
	public class BougerCarte implements MouseListener{
	
		public void mouseClicked(MouseEvent arg0) {
			int taille =  (int) (Math.ceil(200.0/tailleP));
			Point p = arg0.getPoint();
			if (p.x > 200 - zoom.x)p.x = 200 - zoom.x;
			if (p.y > 200 - zoom.y)p.y = 200 - zoom.y;
			if (p.x < zoom.x)p.x = zoom.x;
			if (p.y < zoom.y)p.y = zoom.y;
			controleur.changerVisionCarte(new Point ((int)(Math.ceil(p.x - zoom.x)/taille), (int)(Math.ceil(p.y - zoom.y)/taille)), false);
		}
		
		
		@Override
		public void mouseEntered(MouseEvent arg0) {
		}
		@Override
		public void mouseExited(MouseEvent arg0) {
		}
		@Override
		public void mousePressed(MouseEvent arg0) {
		}
		@Override
		public void mouseReleased(MouseEvent arg0) {
		}
	}
}
