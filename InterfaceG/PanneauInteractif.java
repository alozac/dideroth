package InterfaceG;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.Controleur;
/**
 * Un panneau permettant de donner les informations relatives à la case où se trouve la souris.
 */
public class PanneauInteractif extends JPanel {
	private Controleur controleur;
	
	JLabel infosTerrain = new JLabel();
	InfosUnite infosUnite = new InfosUnite();
	/**
	 * Permet de créer le panneau où se trouve les informations relatives à la case où se trouve la souris.
	 * @param control est un Controleur qui correspond au controleur relié au modèle.
	 */
	public PanneauInteractif(Controleur control){
		controleur = control;
		
		setLayout(new BorderLayout());

		infosTerrain.setHorizontalAlignment(JLabel.CENTER);
		
		add(infosTerrain, BorderLayout.NORTH);
		add(infosUnite, BorderLayout.CENTER);
		add (Box.createVerticalStrut(5), BorderLayout.SOUTH);
	}
	/**
	 * Permet de mettre à jour le PanneauInteractif.
	 * @param x correspond à la coordonnée x de la case où se trouve la souris.
	 * @param y correspond à la coordonnée y de la case où se trouve la souris.
	 */
	public void miseAJour(int x, int y) {
		String franchiss = controleur.plateau.getCase(x, y).estFranchissable()?"Oui" : "Non";
		infosTerrain.setText("<html>(" + x + " , " + y + ")  Franchissable : " + franchiss);
		infosUnite.miseAJour(x, y);
	}
	/**
	 * Permet d'afficher les informations concernant les unités, si la souris se trouve sur une case contenant une unité.
	 */
	public class InfosUnite extends JPanel{
		JLabel typeUnite = new JLabel();
		JLabel imUnit = new JLabel();
		String path = new File("").getAbsolutePath();
		ImageIcon[] imgs = {new ImageIcon(path +"/Images/vie.png"), new ImageIcon(path +"/Images/attaque.png"),
							new ImageIcon(path +"/Images/portees.png"), new ImageIcon(path +"/Images/esquive.png"),
							new ImageIcon(path +"/Images/production.png")};
		JLabel[] JLabCaract = new JLabel[imgs.length];
		/**
		 * Permet de créer le panneau InfosUnite.
		 */
		public InfosUnite(){
			setLayout(new BorderLayout());
			
			typeUnite.setHorizontalAlignment(JLabel.CENTER);
			imUnit.setPreferredSize(new Dimension(100,100));
			
			JPanel infos = new JPanel();
			infos.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
			
			JPanel caract = new JPanel();
			caract.setLayout(new BoxLayout(caract, BoxLayout.Y_AXIS));
			
			creerJLabelCaract();
			for (int i = 0 ; i < JLabCaract.length ; i++)
				caract.add(JLabCaract[i]);

			infos.add(imUnit);
			infos.add(caract);
			
			add(typeUnite, BorderLayout.NORTH);
			add(infos, BorderLayout.CENTER);
			
		}
		/**
		 * Permet de mettre à jour le panneau InfosUnite.
		 * @param x correspond à la coordonnée x de la case où se trouve la souris.
		 * @param y correspond à la coordonnée y de la case où se trouve la souris.
		 */
		public void miseAJour(int x, int y) {
			typeUnite.setText("<html><h2>" + controleur.getTypeUnit(x, y) + "<h2></html>");

			ImageIcon im = controleur.determinerImageOcc(x, y, true);
			ImageIcon im2 = null;
			if (im != null) im2 = new ImageIcon(im.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
			imUnit.setIcon(im2);
			
			String[] caract = controleur.getCaractUnite(x, y);
			for (int i = 0 ; i < JLabCaract.length ; i++)
				JLabCaract[i].setText(caract[i]);
		}
		/**
		 * Permet de créer le JLabel où l'on trouve les images correspondant aux caractéristiques de l'unité.
		 */
		private void creerJLabelCaract() {
			for (int i = 0 ; i < imgs.length ; i++){
				ImageIcon im = new ImageIcon(imgs[i].getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
				JLabCaract[i] = new JLabel(im);
				if (i==0) JLabCaract[i].setToolTipText("Points de vie restants / points de vie maximum");
				else if (i==1) JLabCaract[i].setToolTipText("Nombre de coups * dégât infligé (soin prodigués pour les Magiciens)");
				else if (i==2) JLabCaract[i].setToolTipText("Portée d'attaque - portée de déplacement");
				else if (i==3) JLabCaract[i].setToolTipText("Chance d'esquiver les coups");
				else if (i==4) JLabCaract[i].setToolTipText("Production par tour");
			}
		}
	}
	

}
