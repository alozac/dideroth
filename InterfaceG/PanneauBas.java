package InterfaceG;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.Controleur;
/**
 * Représente le panneau qui se situe en bas de l'écran de jeu.
 */
public class PanneauBas extends JPanel {
	EcranJeu ecranJeu;
	
	ImageIcon [] couleur = new ImageIcon[2];
	JButton amelioration =  new JButton("Ameliorer");
	JButton achat = new JButton("Acheter");
	JLabel nbTours;
	JLabel cpteurMonstres = new JLabel();
	JLabel joueur = new JLabel();
	JLabel nivChateau;
	JLabel or;
	JLabel bois;
	JLabel minerai;
	
	Controleur controleur;
	/**
	 * Permet de créer le panneau en bas de l'écran de jeu.
	 * @param ecr est un EcranJeu, le panneauBas que l'on crée se situera en bas de celui-ci.
	 * @param control est un Controleur qui correspond au controleur relié au modèle.
	 */
	public PanneauBas(EcranJeu ecr, Controleur control){
		ecranJeu = ecr;
		
		controleur = control;
		BoxLayout l = new BoxLayout(this, BoxLayout.X_AXIS);
		setLayout(l);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));

		String s = new File("").getAbsolutePath();
		couleur[0] = new ImageIcon(s+"/Images/Bleu.png");
		couleur[1] = new ImageIcon(s+"/Images/Rouge.png");
		nbTours = new JLabel(new ImageIcon(s+"/Images/finTour.png"));
		nivChateau = new JLabel(new ImageIcon(s+"/Images/icone_chateau.png"));
		or = new JLabel("", new ImageIcon(s+"/Images/po.png"), JLabel.CENTER);
		bois = new JLabel("", new ImageIcon(s+"/Images/bois.png"), JLabel.CENTER);
		minerai = new JLabel("", new ImageIcon(s+"/Images/pierre.png"), JLabel.CENTER);

		miseAJourLabels();
		
		add(Box.createGlue());
		add (nbTours);
		nbTours.setToolTipText("nombre de tour joué");
		
		if (controleur.jeuCoop()){
			add(Box.createGlue());
			add (cpteurMonstres);
			ImageIcon im = new ImageIcon(new File("").getAbsolutePath()+"/Images/monstre.png");
			cpteurMonstres.setIcon(new ImageIcon(im.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT)));
			cpteurMonstres.setToolTipText("nombre de monstres restants");
		}
		add(Box.createGlue());
		add (joueur);
		joueur.setToolTipText("la couleur et la tribu du joueur courant");
		add(Box.createGlue());
		JPanel ressources = new JPanel();
		ressources.add(or);
		ressources.add(bois);
		ressources.add(minerai);
		add(ressources);
		add(Box.createGlue());
		add(achat);
		add(Box.createGlue());
		add(amelioration);
		add (nivChateau);
		add(Box.createGlue());
			
		amelioration.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				ecranJeu.setFocusable(true);
				ecranJeu.requestFocus();
				controleur.ameliorer();
			}
		});
		
		achat.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				controleur.annulerAction(); 
				new FenetreAchat(ecranJeu, controleur);
				ecranJeu.setFocusable(true);
				ecranJeu.requestFocus();
			}
		});
		amelioration.setToolTipText("Augmente de 1 le niveau du château et des autres bâtiments. Coût pour le niveau suivant : Or :"+controleur.getChateau().getCout().get("Or")+" Bois : "+controleur.getChateau().getCout().get("Bois")+" Minerai : "+controleur.getChateau().getCout().get("Minerai")+".");
		achat.setToolTipText("Vous pouvez acheter des bâtiments et des unités");
		
		JPanel changemtUnit = new JPanel();
		changemtUnit.setLayout(new BoxLayout(changemtUnit, BoxLayout.X_AXIS));
		JButton gauche =new JButton("<");
		JButton droite = new JButton (">");
		gauche.setToolTipText("Ces boutons permettent de naviguer entre les unités qui n'ont pas encore attaqué");
		droite.setToolTipText("Ces boutons permettent de naviguer entre les unités qui n'ont pas encore attaqué");
		
		changemtUnit.add(gauche);
		changemtUnit.add(droite);
		
		ActionListener changeUnit = new ActionListener(){
			public void actionPerformed(ActionEvent event){
				String direction = ((AbstractButton) event.getSource()).getText();
				controleur.changeUnit(direction);
			}
		};
		
		gauche.addActionListener(changeUnit);
		droite.addActionListener(changeUnit);
		add(changemtUnit);
		add(Box.createGlue());	
	}
	/**
	 * Permet de mettre à jour les boutons et les labels du PanneauBas en appelant miseAJourBoutons() et miseAJourLabels().
	 */
	public void miseAJour(){
		miseAJourLabels();
		miseAJourBoutons();
	}
	/**
	 * Permet de mettre à jour les labels du PanneauBas.
	 */
	public void miseAJourLabels(){
		String couleurJ = controleur.getCouleur();
		
		joueur.setIcon(couleur[couleurJ.equals("Bleu")? 0:1]);
		joueur.setText(controleur.getTribu());
		
		nivChateau.setText(""+ controleur.getChateau().getNiveau());
		
		or.setText(controleur.getRessources("Or"));
		minerai.setText(controleur.getRessources("Minerai"));
		bois.setText(controleur.getRessources("Bois"));
		
		nbTours.setText(controleur.getNumTour());
		cpteurMonstres.setText(controleur.getNbMonstres());
	}
	/**
	 * Permet de mettre à jour les boutons du PanneauBas.
	 */
	public void miseAJourBoutons(){
		if (!controleur.amelChateauPossible()) amelioration.setEnabled(false);
		else amelioration.setEnabled(true);
		amelioration.setToolTipText("Augmente de 1 le niveau du château et des autres bâtiments. Coût pour le niveau suivant : Or :"+controleur.getChateau().getCout().get("Or")+" Bois : "+controleur.getChateau().getCout().get("Bois")+" Minerai : "+controleur.getChateau().getCout().get("Minerai")+".");
	}

	

}
