package InterfaceG;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


import Controleur.Controleur;

public class EcranAccueil extends JFrame {
    
    Controleur controleur;
    PanneauHaut panh= new PanneauHaut();
    PanneauPrincipal panp = new PanneauPrincipal();
    
    class PanneauPrincipal extends JPanel {
        JButton jouer = new JButton("Jouer");
    /**
     * Permet de creer le panneau principal où se trouve le bouton "jouer".    
     */
        public PanneauPrincipal() {
            this.setLayout(new GridLayout(1,1));
            setPreferredSize(new Dimension(400,50));
            this.setBorder(new LineBorder(Color.GRAY));
            this.jouer.setPreferredSize(new Dimension(80, 80));
            this.setOpaque(false);
            this.setBackground(new Color(1f,0f,0f,0f));
            
            
            this.add(jouer);
            
            jouer.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent event) {
                    EcranAccueil.this.dispose();
                    EcranOptions ecran = new EcranOptions(controleur);
                    ecran.setVisible(true);
                }
            });
        }
    }
    
    class PanneauHaut extends JPanel {
        JButton regles = new JButton("Regles");
        /**
         * Permet de cr�er le panneau en haut de l'�cran d'accueil, o� se trouve le bouton "R�gles".
         */
        public PanneauHaut() {
            BorderLayout b = new BorderLayout();
            this.setLayout(b);
            this.setOpaque(false);
            this.setBackground(new Color(1f,0f,0f,0f));
            
            setPreferredSize(new Dimension(100,50));
            
            this.add(regles, BorderLayout.EAST);
            regles.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent event) {
                    // envoie a l'ecran d'informations
                    Regles reg = new Regles();
                    reg.setVisible(true);
                }
            });
        }
    }
    
    public class ImagePanel extends JPanel {
        
        private Image img;
        /**
         * Permet de cr�er l'image qui servira de fond d'�cran � l'�cran d'accueil.
         * @param img est une Image qui servira de fond d'�cran � l'�cran d'accueil.
         */
        public ImagePanel(Image img){
            this.img = img;
            this.setVisible(true);
        }
        
        public void paintComponent(Graphics g) {
            g.drawImage(img, 0, 0, null);
        }
    }
    /**
     * Permet de cr�er l'�cran d'accueil.
     * @param control est un Controleur qui correspond au controleur reli� au mod�le.
     */
    public EcranAccueil(Controleur control) {
        this.setTitle("Dideroth - ecran d'accueil");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // this.setLocationRelativeTo(null);
        this.setMinimumSize(new Dimension(700,700));
        
        BorderLayout b = new BorderLayout();
        b.setHgap(1);
        JPanel container = new JPanel(b);
        container.add(panp, BorderLayout.CENTER);
        container.add(panh, BorderLayout.NORTH);
        container.setOpaque(false);
        container.setBackground(new Color(1f,0f,0f,0f));
        controleur = control;
        
        this.setContentPane(new ImagePanel(new ImageIcon(controleur.chemin + "Images/foret.jpg").getImage()));
        
        
        this.getContentPane().add(container);
        this.pack();
        this.setVisible(true);

    }
    
}
