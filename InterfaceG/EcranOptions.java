package InterfaceG;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Controleur.Controleur;

public class EcranOptions extends JFrame{
    PanneauOptions pano;
    PanneauEnBas panb;
    
    Controleur controleur;
    /**
     * Permet de créer l'écran d'option de jeu.
     * @param control est un Controleur qui correspond au controleur relié au modèle.
     */
    public EcranOptions(Controleur control){
        this.setTitle("Dideroth - options de jeu");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //  this.setLocationRelativeTo(null);
        this.setMinimumSize(new Dimension(700,700));
        controleur = control;
        pano = new PanneauOptions(controleur);
        panb = new PanneauEnBas();
        JPanel container = new JPanel(new GridLayout(2,1));
        container.add(pano);
        container.add(panb);
        container.setOpaque(false);
        container.setBackground(new Color(1f,0f,0f,0f));
        this.setContentPane(new ImagePanel(new ImageIcon(controleur.chemin + "/Images/fond.jpg").getImage()));
        

        
        this.getContentPane().add(container);
        this.pack();
        this.setVisible(true);

    }
    
    public class ImagePanel extends JPanel {
        
        private Image img;
        /**
         * Permet de créer une image qui sera le fond d'écran de l'écran d'options.
         * @param img est une Image qui correspond à l'image choisie pour être le fond d'écran pour l'écran d'option.
         */
        public ImagePanel(Image img){
            this.img = img;
            this.setVisible(true);
        }
        
        public void paintComponent(Graphics g) {
            g.drawImage(img, 0, 0, null);
        }
    }

    
    class PanneauEnBas extends JPanel {
        JButton annuler = new JButton("Annuler");
        JButton jouer = new JButton("Confirmer");
        /**
         * Permet de créer le panneau en bas de l'écran, où se trouve les boutons "annuler" et "confirmer".
         */
        public PanneauEnBas() {
            this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
            //setPreferredSize(new Dimension(500,100));
            
            this.setOpaque(false);
            this.setBackground(new Color(1f,0f,0f,0f));
            
            
            this.add(annuler);
            this.add(jouer);
            
            
            annuler.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent event) {
                    EcranOptions.this.dispose();
                    EcranAccueil ecran = new EcranAccueil(controleur);
                    ecran.setVisible(true);
                }
            });
            
            jouer.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent event) {
                    if (pano.tribuJ1Sel != null && pano.tribuJ2Sel != null
                    && pano.carteSel != null && pano.typeJeu != null){
                        EcranOptions.this.dispose();
	            		controleur.creationJeu(pano.tribuJ1Sel.getText(), pano.tribuJ2Sel.getText(),
	            								pano.carteSel.getText(), pano.typeJeu.getText(), pano.paramModeCoop);
	                    EcranJeu ecran = new EcranJeu(controleur);
	                    ecran.setVisible(true);
                    }
                }
                
            });
        }
    }
    
}
