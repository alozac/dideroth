package InterfaceG;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.File;
import Controleur.Controleur;
/**
 * Représente le panneau qui se situe à droite de l'écran de jeu.
 */
public class PanneauDroit extends JPanel {
	Controleur controleur;
	EcranJeu ecranJeu;
	public InfosTour infosTour;
	JButton quitter = new JButton("Quitter");
	JButton finTour = new JButton("Fin du tour", new ImageIcon(new File("").getAbsolutePath() + "/Images/finTour.png"));
	int largeurPan;
	/**
	 * Permet de créer le panneau à droite de l'écran de jeu.
	 * @param ecrJ est un EcranJeu, le panneau que l'on crée sera à droite de celui-ci.
	 * @param control est un Controleur, il correspond au controleur relié au modèle.
	 */
	public PanneauDroit(EcranJeu ecrJ, Controleur control){
		controleur = control;
		ecranJeu = ecrJ;

		setLayout( new BorderLayout () );

		largeurPan = controleur.determLargPanB() + 2;
		setPreferredSize(new Dimension(largeurPan ,850));
		
		infosTour = new InfosTour();
		finTour.setPreferredSize(new Dimension(largeurPan, 60));
		
		add(quitter, BorderLayout.NORTH);
		add(finTour, BorderLayout.SOUTH);
		add(infosTour, BorderLayout.CENTER);
		
		quitter.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent event){
				JOptionPane conf = new JOptionPane();
				int option = conf.showConfirmDialog(null, "Voulez-vous vraiment quitter?", "Quitter",
													JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (option == JOptionPane.YES_OPTION) {
                    ecranJeu.dispose();
                    controleur.quitter();
                    EcranAccueil ecran = new EcranAccueil(controleur);
                    ecran.setVisible(true);
				}
				ecranJeu.setFocusable(true);
				ecranJeu.requestFocus();
				
			  }
		});
		
		finTour.addActionListener(new ActionListener(){
			  public void actionPerformed(ActionEvent event){
				  	controleur.finTour();
					ecranJeu.setFocusable(true);
					ecranJeu.requestFocus();
			  }
		});
		
	}
/**
 * Permet de mettre à jour le PanneauDroit.
 */
	public void miseAJour() {
		if (controleur.getNbPARestants() == 0) finTour.setBackground(Color.GREEN);
		else finTour.setBackground(null);
		infosTour.miseAJour();
	}
	/**
	 * Permet de mettre à jour le panneau interactif où l'on trouve les informations de la case sur laquelle la souris se trouve.
	 * @param x correspond à la coordonnée x de la case où se trouve la souris.
	 * @param y correspond à la coordonnée y de la case où se trouve la souris.
	 */
	public void miseAJourPanInter(int x, int y) {
		infosTour.panInter.miseAJour(x,y);
	}
	/**
	 * Permet d'ajouter un combat dans le rapport de combat.
	 * @param rapport une chaine de caracteres résumant le combat à afficher
	 */
	public void ajouterCombat(String rapport) {
		infosTour.rap.ajouterCombat(rapport);
	}
	/**
	 * Permet d'afficher les informations relatives au jeu (minimap, points d'action restants, combats, etc).
	 */
	class InfosTour extends JPanel{
		Minimap minimap;
		JSlider zoom;
		PanneauInteractif panInter= new PanneauInteractif(controleur);
		RapportsCombats rap = new RapportsCombats(controleur);
		JLabel pA = new JLabel();
		/**
		 * Permet de créer le panneau où l'on trouve les informations du jeu.
		 */
		public InfosTour(){
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createLineBorder(Color.BLACK));
			
			minimap = new Minimap(controleur);
			zoom = creerCurseur();
			
			JPanel jp = new JPanel();
			jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
			jp.add(minimap);
			jp.add(zoom);
			jp.add(panInter);
			
			pA.setHorizontalAlignment(JLabel.CENTER);
			
			add(jp, BorderLayout.NORTH);
			add(rap, BorderLayout.CENTER);
			add(pA, BorderLayout.SOUTH);
			pA.setToolTipText("Les points d'action définissent le nombre d'action que vous pouvez effectuer en un tour.");
			miseAJour();
		}
		/**
		 * Permet de mettre à jour le panneau "InfosTour" (points d'action, minimap, etc).
		 */
		public void miseAJour() {
			String coulTxt = controleur.getNbPARestants()==0?" color=red":"";
			pA.setText("<html><center>Points d'actions restants:<br><FONT" + coulTxt+" size=6>"+
						controleur.getNbPARestants()+"</FONT></center></html>");
			minimap.repaint();
			zoom.setValue(ecranJeu.carte.getZoomL());
		}

		/**
		 * Permet de créer le curseur qui permet de zoomer/dézoomer sur la minimap.
		 * @return un JSlider, qui sera le curseur qui permet de zoomer/dézoomer sur la minimap.
		 */
		public JSlider creerCurseur(){
			int tailleP = controleur.plateau.taille;
			JSlider j = new JSlider(10 , tailleP<50?tailleP:50, controleur.getZoomCarte().x);
			j.setToolTipText("Change le nombre de cases visibles en largeur");
			j.setPaintTicks(true);
			j.setPaintLabels(true);
			j.setMajorTickSpacing(10);
			j.setMinorTickSpacing(5);
			j.addChangeListener(new ChangeListener(){
				@Override
				public void stateChanged(ChangeEvent e) {
					JSlider zm = ((JSlider) e.getSource());
					controleur.changeZoom(zm.getValue());
					ecranJeu.setFocusable(true);
					ecranJeu.requestFocus();
					//miseAJour();
				}
			});
			return j;
		}
	}
	

}
