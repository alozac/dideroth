package InterfaceG;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Controleur.Controleur;

public class PanneauOptions extends JPanel {
	Controleur controleur;
    PanneauH haut;
    PanneauB bas;
    JRadioButton tribuJ1Sel, tribuJ2Sel, carteSel, typeJeu;
    int[] paramModeCoop = {10, 10};
    
    public PanneauOptions(Controleur control) {
        this.setLayout(new GridLayout(2,1));
        controleur = control;
        haut = new PanneauH(controleur);
        bas = new PanneauB(controleur);
        this.add(haut);
        this.add(bas);
        this.setOpaque(false);
        this.setBackground(new Color(1f,0f,0f,0f));
    }
    
    class PanneauH extends JPanel {
    		Controleur controleur;
        PanneauDiff diff;
        PanneauVS vs;
        
        public PanneauH(Controleur control) {
            setPreferredSize(new Dimension(600,175));
            this.setLayout(new GridLayout(1,2));
            controleur = control;
            diff = new PanneauDiff(controleur);
            vs = new PanneauVS(controleur);
            this.add(diff);
            this.add(vs);
            this.setVisible(true);
            this.setOpaque(false);
            this.setBackground(new Color(1f,0f,0f,0f));
        }
        
        public class PanneauVS extends JPanel {
            Controleur controleur;
            
            public PanneauVS(Controleur control) {
                this.setLayout(new GridLayout(4,1));
                controleur = control;
                final JRadioButton tribu1 = new JRadioButton("Humain");
                final JRadioButton tribu2 = new JRadioButton("Nain");
                final JRadioButton tribu3 = new JRadioButton("Elfe");
                tribu1.setToolTipText("Les Humains ont un avantage dans le combat au corps à corps");
                tribu2.setToolTipText("Les Nains ont un avantage d'endurance (leur points de vie sont augmentés)");
                tribu3.setToolTipText("Les Elfes ont un avantage dans le combat à distance");
                final JLabel titre = new JLabel("Joueur 1 :");
                this.setBorder(new LineBorder(Color.gray));
                this.setOpaque(false);
                this.setBackground(new Color(1f,0f,0f,0f));
                
                
                
                
                
                ActionListener actionlistener = new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        JRadioButton button = (JRadioButton) event.getSource();
                            button.setSelected(true);
                            tribuJ1Sel = button;
                    }
                };
                
                tribu1.addActionListener(actionlistener);
                tribu2.addActionListener(actionlistener);
                tribu3.addActionListener(actionlistener);
                tribu1.setForeground(java.awt.Color.white);
                tribu1.setOpaque(false);
                tribu2.setForeground(java.awt.Color.white);
                tribu2.setOpaque(false);
                tribu3.setForeground(java.awt.Color.white);
                tribu3.setOpaque(false);
                titre.setForeground(java.awt.Color.white);
                tribu1.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                tribu2.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                tribu3.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                //titre.setFont(new Font(Font.DIALOG, Font.ITALIC, 10));
                
                
                ButtonGroup group = new ButtonGroup();
                group.add(tribu1);
                group.add(tribu2);
                group.add(tribu3);
                
                this.add(titre);
                this.add(tribu1);
                this.add(tribu2);
                this.add(tribu3);
                this.setVisible(true);
            }
            
        }
        
        class PanneauDiff extends JPanel {
            Controleur controleur;
            JSlider[] diffCoop = new JSlider[2];
            JPanel jpModeCoop = new JPanel(); 
            
            public PanneauDiff(Controleur control) {
                this.setBorder(new LineBorder(Color.gray));
                this.setOpaque(false);
                this.setBackground(new Color(1f,0f,0f,0f));
                
            	controleur = control;
                JRadioButton PVP = new JRadioButton("<html><font color=white>Joueur versus Joueur</font></html>");
                JRadioButton PVE = new JRadioButton("<html><font color=white>Joueurs versus Ennemi</font></html>");
                
                jpModeCoop.setVisible(false);
                jpModeCoop.setAlignmentX(0);
                
                ActionListener actionListModeJeu = new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        JRadioButton button = (JRadioButton) event.getSource();
                        button.setSelected(true);
                        typeJeu=button;
                        if (button == PVE)
                            jpModeCoop.setVisible(true);
                        else
                            jpModeCoop.setVisible(false);
                    }
                };
                
                HashMap<JSlider, JLabel> valSliders = new HashMap<JSlider, JLabel>();         
                ChangeListener actionParamModeCoop = new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e) {
                    	JSlider src = (JSlider) e.getSource();
                    	valSliders.get(src).setText("<html><font color=white>"+src.getValue() + "</font></html>");
                    	if (src == diffCoop[0]) paramModeCoop[0] = src.getValue();
                    	else paramModeCoop[1] = src.getValue();
					}
                };
                
                final JLabel titre = new JLabel("<html><font color=white>Mode de jeu :</font></html>");
                PVP.setToolTipText("Le joueur bleu et le joueur rouge s'affrontent, il n'y a pas d'ennemi neutre. Celui qui conquiert le chateau de l'autre remporte la partie.");
                PVE.setToolTipText("Le joueur bleu et le joueur rouge peuvent s'attaquer, mais le but de la partie est d'éliminer tous les ennemis neutres. Il y a un nombre de tour limité.");   
                
                PVP.addActionListener(actionListModeJeu);
                PVE.addActionListener(actionListModeJeu);             
                
                ButtonGroup groupModeJeu = new ButtonGroup();
                groupModeJeu.add(PVP);
                groupModeJeu.add(PVE);
               
                this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
                this.add(titre);
                this.add(PVP);
                this.add(PVE);
               
                jpModeCoop.setLayout(new BoxLayout(jpModeCoop, BoxLayout.Y_AXIS));
                creerPanneauModeCoop(valSliders, actionParamModeCoop);
                
                Font font = new Font(Font.DIALOG, Font.ITALIC, 15);
                this.add(jpModeCoop);
                jpModeCoop.setOpaque(false);
                PVP.setOpaque(false);
                PVE.setOpaque(false);
                titre.setOpaque(false);
                PVP.setFont(font);
                PVE.setFont(font);
                diffCoop[0].setToolTipText("Nombre de monstres présents sur la carte, qu'il faut éliminier pour gagner la partie");
                diffCoop[1].setToolTipText("Nombre de tours dont vous disposez pour terminer la partie");
                
                this.setVisible(true);  
            }
            
            public  void creerPanneauModeCoop(HashMap<JSlider, JLabel> valSliders, ChangeListener actionParamModeCoop) {
            	JLabel[] lab = new JLabel[2];
        		JCheckBox checkToursIll = new JCheckBox("<html><font color=white>Illimité</font></html>");
        		checkToursIll.addItemListener(new ItemListener(){
					@Override
					public void itemStateChanged(ItemEvent e) {
						if (checkToursIll.isSelected()){
							diffCoop[1].setEnabled(false);
							lab[1].setText("<html><font color=white>inf</font></html>");
						}else diffCoop[1].setEnabled(true);
						paramModeCoop[1] = -1;
					}
        		});
        		checkToursIll.setOpaque(false);
				for (int i = 0 ; i < diffCoop.length ; i++){
                	JPanel jp = new JPanel();
                	jp.setOpaque(false);
                	diffCoop[i] = new JSlider(1, 50, 10);
                    diffCoop[i].setOpaque(false);
                	lab[i] = new JLabel("<html><font color=white>"+diffCoop[i].getValue() + "</font></html>");
                    valSliders.put(diffCoop[i], lab[i]);
                	diffCoop[i].addChangeListener(actionParamModeCoop);
                	jp.add(lab[i]);
                    jp.add(diffCoop[i]);
                    
                    jpModeCoop.add(jp);
				}
            	jpModeCoop.add(checkToursIll);
            }
            
        }
    }
    
    class PanneauB extends JPanel {
    	Controleur controleur;
        PanneauCarte carte;
        PanneauTribu tribu;
        
        public PanneauB(Controleur control) {
        	controleur = control;
            setPreferredSize(new Dimension(600,100));
            this.setLayout(new GridLayout(1,2));
            carte = new PanneauCarte(controleur);
            tribu = new PanneauTribu(controleur);
            this.add(carte);
            this.add(tribu);
            this.setOpaque(false);
            this.setBackground(new Color(1f,0f,0f,0f));
            
            
            this.setVisible(true);
            
        }
        
        class PanneauTribu extends JPanel {
            Controleur controleur;
            
            public PanneauTribu(Controleur control) {
                controleur = control;
                this.setLayout(new GridLayout(4,1));
                final JRadioButton tribu1 = new JRadioButton("Humain");
                final JRadioButton tribu2 = new JRadioButton("Nain");
                final JRadioButton tribu3 = new JRadioButton("Elfe");
                tribu1.setToolTipText("Les Humains ont un avantage dans le combat au corps à corps");
                tribu2.setToolTipText("Les Nains ont un avantage d'endurance (leur points de vie sont augmentés)");
                tribu3.setToolTipText("Les Elfes ont un avantage dans le combat à distance");
                final JLabel titre = new JLabel("Joueur 2 :");
                this.setBorder(new LineBorder(Color.gray));
                this.setOpaque(false);
                this.setBackground(new Color(1f,0f,0f,0f));
                
                
                
                
                
                
                ActionListener actionlistener = new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        JRadioButton button = (JRadioButton) event.getSource();
                        button.setSelected(true);
                        tribuJ2Sel = button;
                    }
                         
                };
                
                
                tribu1.addActionListener(actionlistener);
                tribu2.addActionListener(actionlistener);
                tribu3.addActionListener(actionlistener);
                tribu1.setForeground(java.awt.Color.white);
                tribu1.setOpaque(false);
                tribu2.setForeground(java.awt.Color.white);
                tribu2.setOpaque(false);
                tribu3.setForeground(java.awt.Color.white);
                tribu3.setOpaque(false);
                titre.setForeground(java.awt.Color.white);
                tribu1.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                tribu2.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                tribu3.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                
                
                
                
                ButtonGroup group = new ButtonGroup();
                group.add(tribu1);
                group.add(tribu2);
                group.add(tribu3);
                
                this.add(titre);
                this.add(tribu1);
                this.add(tribu2);
                this.add(tribu3);
                this.setVisible(true);
                
            }
            
        }
        
        class PanneauCarte extends JPanel {
            Controleur controleur;

            public PanneauCarte(Controleur control) {
            		controleur = control;
                final JRadioButton desert = new JRadioButton("Desert");
                final JRadioButton foret = new JRadioButton("Foret");
                final JRadioButton marecage = new JRadioButton("Montagne");
                final JLabel titre = new JLabel("Carte :");
                this.setBorder(new LineBorder(Color.gray));
                this.setOpaque(false);
                this.setBackground(new Color(1f,0f,0f,0f));
                
                
                
                
                ActionListener actionlistener = new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        JRadioButton button = (JRadioButton) event.getSource();
                        button.setSelected(true);
                        carteSel = button;
                    }
                };
                
                
                desert.addActionListener(actionlistener);
                foret.addActionListener(actionlistener);
                marecage.addActionListener(actionlistener);
                desert.setForeground(java.awt.Color.white);
                desert.setOpaque(false);
                marecage.setForeground(java.awt.Color.white);
                marecage.setOpaque(false);
                foret.setForeground(java.awt.Color.white);
                foret.setOpaque(false);
                titre.setForeground(java.awt.Color.white);
                titre.setOpaque(false);
                desert.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                foret.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                marecage.setFont(new Font(Font.DIALOG, Font.ITALIC, 15));
                
                
                
                ButtonGroup group = new ButtonGroup();
                group.add(desert);
                group.add(foret);
                group.add(marecage);
                
                
                this.setLayout(new GridLayout(4,1));
                this.add(titre);
                this.add(desert);
                this.add(foret);
                this.add(marecage);
                
                this.setVisible(true);
                
                
                
            }
            
        }
    }
    
}
