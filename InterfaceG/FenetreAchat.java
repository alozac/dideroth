package InterfaceG;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import Controleur.Controleur;

public class FenetreAchat extends JDialog {
	Controleur controleur;
	AchatUnites achUni;
	AchatBat achBat;
    String coulJoueur;
	/**
	 * Permet de créer la fenêtre d'achat, où l'on peut acheter des unités et des bâtiments.
	 * @param parent est une JFrame correspond à l'écran auquel la fenêtre appartient.
	 * @param control est un Controleur qui correspond au controleur relié au modèle.
	 */
	public FenetreAchat(JFrame parent, Controleur control){
		super(parent, true);

		controleur = control;
		controleur.annulerAction();
		
		this.setTitle("Dideroth - Boutique");
	    this.setLocationRelativeTo(parent);
	    this.setMinimumSize(new Dimension(400,450));
	    //setLayout(new BorderLayout());
	    
	    JPanel container = new JPanel(new BorderLayout());
	    
	    coulJoueur = controleur.getCouleur().equals("Rouge")?"red":"blue";
	    achUni = new AchatUnites();
	    achBat = new AchatBat();
	    
	    JTabbedPane onglets = new JTabbedPane();
	    onglets.add("Unites", achUni);
	    onglets.add("Batiments", achBat);
	    
	    container.add(onglets);
	    
	    Toolkit tk = Toolkit.getDefaultToolkit();
		Image img = tk.getImage(controleur.chemin + "Images/curseur.png");
		Cursor monCurseur = tk.createCustomCursor(img, new Point(0,0), "");
		setCursor(monCurseur);
	    
	    this.setContentPane(container);
	    this.setVisible(true);
	}
	
	
	public class AchatBat extends JPanel{
		JRadioButton batSel;
		String[] nbBat = {"scierie", "mine", "tour"};
		/**
		 * Permet de créer l'onglet où l'on peut acheter des bâtiments.
		 */
		public AchatBat(){
			setLayout(new BorderLayout());
			
			JPanel panneauC=new JPanel();
			JPanel panneauBas=new JPanel();
			
			JButton valider=new JButton ("Acheter");
			JButton annuler=new JButton ("Annuler");
			panneauBas.add(annuler);
			panneauBas.add(valider);
			
			ActionListener boutonSel = new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
	             if (batSel != null) batSel.setBorderPainted(false);
               	 batSel = ((JRadioButton) e.getSource());
               	 batSel.setBorderPainted(true);
				}
			};
			
			ButtonGroup gr = new ButtonGroup();
		    String chemin = controleur.chemin + "Images/";
			for (int i = 0 ; i<nbBat.length;i++){
				ImageIcon imBat = new ImageIcon(chemin + nbBat[i] + "Entier.png");
				ImageIcon imBat2 = new ImageIcon(imBat.getImage().getScaledInstance(140, 140, Image.SCALE_DEFAULT));
				JRadioButton but = new JRadioButton(nbBat[i], imBat2);
				HashMap <String, Integer> r = controleur.getCoutToolTipB(nbBat[i]);
				but.setToolTipText("<html>Or : "+r.get("Or")+ "<br>Bois : "+r.get("Bois")+ "<br>Minerai : "+ r.get("Minerai")
									+"<br>Niveau du château requis : " + r.get("NiveauNece") + "</html>");
           	 	but.setBorder(BorderFactory.createLineBorder(coulJoueur.equals("blue")?Color.BLUE : Color.RED));
				but.setIconTextGap(0);
				but.setHorizontalTextPosition(SwingConstants.CENTER);
				but.setVerticalTextPosition(SwingConstants.BOTTOM);
				gr.add(but);
				but.setEnabled(controleur.achatBatPossible(nbBat[i]));
				panneauC.add(but);
				but.addActionListener(boutonSel);
			}
			
			ActionListener actionVal = new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == valider && batSel != null){
						controleur.setActionEnCours(batSel.getText());
					}	
					FenetreAchat.this.dispose();
				}	
			};
			
			valider.addActionListener(actionVal);
			annuler.addActionListener(actionVal);
			
			add(panneauBas, BorderLayout.SOUTH);
			add(panneauC, BorderLayout.CENTER);
			
			
		}
	}
	
	public class AchatUnites extends JPanel{
		
		JRadioButton unitSel;
		int nbRecrutMax;
		int qteSel;
		String[] typeUnit = {"Chevalier", "Archer", "Magicien", "Machine de guerre", "Dragon"};
		/**
		 * Permet de créer l'onglet où l'on peut acheter des unités.
		 */
		public AchatUnites(){		
			setLayout(new BorderLayout());
			
		    JPanel panneauCentre = new JPanel(new BorderLayout());
		    JPanel listUnit = new JPanel(new GridLayout(0,2));
		    JScrollPane jsp = new JScrollPane(listUnit);
		    
		    JPanel panneauBas = new JPanel();
		    JButton valider = new JButton("Acheter");
		    JButton annuler = new JButton("Annuler");  
		    panneauBas.add(annuler);
		    panneauBas.add(valider);
		
			nbRecrutMax = 0;
			JPanel incrBut = new JPanel();
			JButton moins = new JButton("-");
			JButton plus = new JButton("+");
			JLabel qte = new JLabel(""+0);
			qte.setBorder(BorderFactory.createLineBorder(Color.BLACK));			
			
			incrBut.add(moins);
			incrBut.add(qte);
			incrBut.add(plus);
            
			 ActionListener boutonSel = new ActionListener() {
                 public void actionPerformed(ActionEvent event) {
                	 if (unitSel != null) unitSel.setBorderPainted(false);
                	 unitSel = (JRadioButton) event.getSource();
                	 unitSel.setBorderPainted(true);
                	 nbRecrutMax = controleur.getNbRecrutPossible(unitSel.getText()); 
         			 qte.setText(""+0);
                 }

			};
			
            ButtonGroup group = new ButtonGroup();
		    String chemin = controleur.chemin + "Images/";
			for (int i = 0 ; i < typeUnit.length ; i++){
				String imName = typeUnit[i].equals("Machine de guerre")? "machine" : typeUnit[i];
				JRadioButton but = new JRadioButton(typeUnit[i], new ImageIcon(chemin + coulJoueur + imName + ".png"));
				HashMap <String, Integer> r = controleur.getCoutToolTipU(imName);
				but.setToolTipText("<html>Or : "+r.get("Or")+ "<br>Bois : "+r.get("Bois")+ "<br>Minerai : "+ r.get("Minerai")
									+"<br>Niveau du château requis : " + r.get("NiveauNece") + "</html>");
				group.add(but);
				but.setIconTextGap(0);
				but.setHorizontalAlignment(JRadioButton.CENTER);
				but.setHorizontalTextPosition(SwingConstants.CENTER);
				but.setVerticalTextPosition(SwingConstants.BOTTOM);
				but.addActionListener(boutonSel);
           	 	but.setBorder(BorderFactory.createLineBorder(coulJoueur.equals("blue")?Color.BLUE : Color.RED));
    			but.setEnabled(controleur.getNbRecrutPossible(typeUnit[i]) > 0 ? true : false);
				listUnit.add(but);
			}
			
			
			 ActionListener increm = new ActionListener() {
                 public void actionPerformed(ActionEvent event) {
            		 qteSel  = Integer.parseInt(qte.getText());
                	 if(event.getSource() == moins)
                		 if (qteSel > 0) qte.setText(""+(qteSel-1));
                     if(event.getSource() == plus)
                		 if (qteSel < nbRecrutMax) qte.setText(""+ (qteSel+1));
            		 qteSel  = Integer.parseInt(qte.getText());
                 }
			};
			
			 ActionListener actionL = new ActionListener() {
                 public void actionPerformed(ActionEvent event) {
                	 if(event.getSource() == valider && unitSel != null)
                		 controleur.recruter(unitSel.getText() , qteSel);
                     FenetreAchat.this.setVisible(false);
                 }
			};
			
			moins.addActionListener(increm);
			plus.addActionListener(increm);
			
			valider.addActionListener(actionL);
			annuler.addActionListener(actionL);
			
			panneauCentre.add(incrBut, BorderLayout.SOUTH);
			panneauCentre.add(jsp, BorderLayout.CENTER);
			
			add(panneauBas, BorderLayout.SOUTH);
			add(panneauCentre, BorderLayout.CENTER);
			
		}
	}
}
