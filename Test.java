import javax.swing.*;

import Controleur.Controleur;
import InterfaceG.*;
public class Test {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            Controleur control = new Controleur();
            @Override
            public void run() {
                new EcranAccueil(control);
            }
        });
    }

}
